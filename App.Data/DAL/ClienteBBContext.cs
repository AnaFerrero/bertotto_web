﻿using ClienteBB.BD.Models;
using Microsoft.EntityFrameworkCore;

namespace ClienteBB.BD.DAL
{
    public class ClienteBBContext : DbContext
    {
        public ClienteBBContext()
        {
        }

        public ClienteBBContext(DbContextOptions<ClienteBBContext> options)
          : base(options)
        {
        }

        public static string ConnectionString { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Rol> Roles { get; set; }
        public virtual DbSet<Empresa> Empresas { get; set; }
        public virtual DbSet<Contacto> Contactos { get; set; }
        public virtual DbSet<Localidad> Localidades { get; set; }
        public virtual DbSet<Idioma> Idiomas { get; set; }
        public virtual DbSet<Novedad> Novedades { get; set; }
        public virtual DbSet<TipoMaquinaria> TipoMaquinarias { get; set; }
        public virtual DbSet<TipoMaterial> TipoMateriales { get; set; }
        public virtual DbSet<Maquinaria> Maquinarias { get; set; }
        public virtual DbSet<Evento> Eventos { get; set; }
        public virtual DbSet<ServiceCap> ServiceCaps { get; set; }
        public virtual DbSet<GaleriaImagen> GaleriaImagenes { get; set; }
        public virtual DbSet<GaleriaDocumento> GaleriaDocumentos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }
    }
}
