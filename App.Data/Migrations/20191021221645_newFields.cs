﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClienteBB.BD.Migrations
{
    public partial class newFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MetaTitle",
                table: "ServiceCaps",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MetaTitle",
                table: "Novedades",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MetaTitle",
                table: "Maquinarias",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nombre",
                table: "GaleriaImagenes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MetaTitle",
                table: "ServiceCaps");

            migrationBuilder.DropColumn(
                name: "MetaTitle",
                table: "Novedades");

            migrationBuilder.DropColumn(
                name: "MetaTitle",
                table: "Maquinarias");

            migrationBuilder.DropColumn(
                name: "Nombre",
                table: "GaleriaImagenes");
        }
    }
}
