﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ClienteBB.BD.Migrations
{
    public partial class eventos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "Novedades");

            migrationBuilder.RenameColumn(
                name: "Fecha_Evento",
                table: "Eventos",
                newName: "Fecha_Hasta_Evento");

            migrationBuilder.RenameColumn(
                name: "Descripcion",
                table: "Eventos",
                newName: "Direccion");

            migrationBuilder.AddColumn<DateTime>(
                name: "Fecha_Desde_Evento",
                table: "Eventos",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IdiomaId",
                table: "Eventos",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Eventos_IdiomaId",
                table: "Eventos",
                column: "IdiomaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Eventos_Idiomas_IdiomaId",
                table: "Eventos",
                column: "IdiomaId",
                principalTable: "Idiomas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Eventos_Idiomas_IdiomaId",
                table: "Eventos");

            migrationBuilder.DropIndex(
                name: "IX_Eventos_IdiomaId",
                table: "Eventos");

            migrationBuilder.DropColumn(
                name: "Fecha_Desde_Evento",
                table: "Eventos");

            migrationBuilder.DropColumn(
                name: "IdiomaId",
                table: "Eventos");

            migrationBuilder.RenameColumn(
                name: "Fecha_Hasta_Evento",
                table: "Eventos",
                newName: "Fecha_Evento");

            migrationBuilder.RenameColumn(
                name: "Direccion",
                table: "Eventos",
                newName: "Descripcion");

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Novedades",
                nullable: true);
        }
    }
}
