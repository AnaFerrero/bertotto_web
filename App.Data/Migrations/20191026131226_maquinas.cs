﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClienteBB.BD.Migrations
{
    public partial class maquinas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Ventaja",
                table: "Maquinarias",
                newName: "UrlManual");

            migrationBuilder.RenameColumn(
                name: "DetalleTecnico",
                table: "Maquinarias",
                newName: "UrlFolleto");

            migrationBuilder.AddColumn<string>(
                name: "Tipo",
                table: "Tipos_Maquinarias",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Provincia",
                table: "ServiceCaps",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Activo",
                table: "Maquinarias",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "DescripcionCorta",
                table: "Maquinarias",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Modelo",
                table: "Maquinarias",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tipo",
                table: "Tipos_Maquinarias");

            migrationBuilder.DropColumn(
                name: "Provincia",
                table: "ServiceCaps");

            migrationBuilder.DropColumn(
                name: "Activo",
                table: "Maquinarias");

            migrationBuilder.DropColumn(
                name: "DescripcionCorta",
                table: "Maquinarias");

            migrationBuilder.DropColumn(
                name: "Modelo",
                table: "Maquinarias");

            migrationBuilder.RenameColumn(
                name: "UrlManual",
                table: "Maquinarias",
                newName: "Ventaja");

            migrationBuilder.RenameColumn(
                name: "UrlFolleto",
                table: "Maquinarias",
                newName: "DetalleTecnico");
        }
    }
}
