﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClienteBB.BD.Migrations
{
    public partial class maquinarias : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Tipo",
                table: "Maquinarias",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tipo",
                table: "Maquinarias");
        }
    }
}
