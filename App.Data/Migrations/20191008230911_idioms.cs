﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ClienteBB.BD.Migrations
{
    public partial class idioms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Novedades_TiposNovedades_TipoNovedadId",
                table: "Novedades");

            migrationBuilder.DropTable(
                name: "TiposNovedades");

            migrationBuilder.RenameColumn(
                name: "TipoNovedadId",
                table: "Novedades",
                newName: "IdiomaId");

            migrationBuilder.RenameIndex(
                name: "IX_Novedades_TipoNovedadId",
                table: "Novedades",
                newName: "IX_Novedades_IdiomaId");

            migrationBuilder.CreateTable(
                name: "Idiomas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Idiomas", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Novedades_Idiomas_IdiomaId",
                table: "Novedades",
                column: "IdiomaId",
                principalTable: "Idiomas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Novedades_Idiomas_IdiomaId",
                table: "Novedades");

            migrationBuilder.DropTable(
                name: "Idiomas");

            migrationBuilder.RenameColumn(
                name: "IdiomaId",
                table: "Novedades",
                newName: "TipoNovedadId");

            migrationBuilder.RenameIndex(
                name: "IX_Novedades_IdiomaId",
                table: "Novedades",
                newName: "IX_Novedades_TipoNovedadId");

            migrationBuilder.CreateTable(
                name: "TiposNovedades",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposNovedades", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Novedades_TiposNovedades_TipoNovedadId",
                table: "Novedades",
                column: "TipoNovedadId",
                principalTable: "TiposNovedades",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
