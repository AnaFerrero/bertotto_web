﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClienteBB.BD.Migrations
{
    public partial class idioma : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdiomaId",
                table: "Maquinarias",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Maquinarias_IdiomaId",
                table: "Maquinarias",
                column: "IdiomaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Maquinarias_Idiomas_IdiomaId",
                table: "Maquinarias",
                column: "IdiomaId",
                principalTable: "Idiomas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Maquinarias_Idiomas_IdiomaId",
                table: "Maquinarias");

            migrationBuilder.DropIndex(
                name: "IX_Maquinarias_IdiomaId",
                table: "Maquinarias");

            migrationBuilder.DropColumn(
                name: "IdiomaId",
                table: "Maquinarias");
        }
    }
}
