﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClienteBB.BD.Migrations
{
    public partial class addestado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Tipo",
                table: "Maquinarias",
                newName: "Estado");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Estado",
                table: "Maquinarias",
                newName: "Tipo");
        }
    }
}
