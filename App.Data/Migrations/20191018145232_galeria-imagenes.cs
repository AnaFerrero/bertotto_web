﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ClienteBB.BD.Migrations
{
    public partial class galeriaimagenes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fecha_Alta",
                table: "ServiceCaps");

            migrationBuilder.CreateTable(
                name: "GaleriaImagenes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NoticiaId = table.Column<int>(nullable: false),
                    MaquinariaId = table.Column<int>(nullable: false),
                    RepuestoId = table.Column<int>(nullable: false),
                    Activa = table.Column<bool>(nullable: false),
                    Principal = table.Column<bool>(nullable: false),
                    Imagen = table.Column<string>(nullable: true),
                    Fecha_Alta = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GaleriaImagenes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GaleriaImagenes");

            migrationBuilder.AddColumn<DateTime>(
                name: "Fecha_Alta",
                table: "ServiceCaps",
                nullable: true);
        }
    }
}
