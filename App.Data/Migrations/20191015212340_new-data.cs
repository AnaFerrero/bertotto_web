﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClienteBB.BD.Migrations
{
    public partial class newdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DescripcionCorta",
                table: "ServiceCaps",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescripcionCorta",
                table: "Novedades",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DescripcionCorta",
                table: "ServiceCaps");

            migrationBuilder.DropColumn(
                name: "DescripcionCorta",
                table: "Novedades");
        }
    }
}
