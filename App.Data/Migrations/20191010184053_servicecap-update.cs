﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ClienteBB.BD.Migrations
{
    public partial class servicecapupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Activo",
                table: "ServiceCaps",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "Fecha_Alta",
                table: "ServiceCaps",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IdiomaId",
                table: "ServiceCaps",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceCaps_IdiomaId",
                table: "ServiceCaps",
                column: "IdiomaId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceCaps_Idiomas_IdiomaId",
                table: "ServiceCaps",
                column: "IdiomaId",
                principalTable: "Idiomas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceCaps_Idiomas_IdiomaId",
                table: "ServiceCaps");

            migrationBuilder.DropIndex(
                name: "IX_ServiceCaps_IdiomaId",
                table: "ServiceCaps");

            migrationBuilder.DropColumn(
                name: "Activo",
                table: "ServiceCaps");

            migrationBuilder.DropColumn(
                name: "Fecha_Alta",
                table: "ServiceCaps");

            migrationBuilder.DropColumn(
                name: "IdiomaId",
                table: "ServiceCaps");
        }
    }
}
