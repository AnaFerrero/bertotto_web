﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ClienteBB.BD.Migrations
{
    public partial class changeseventos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fecha_Alta",
                table: "Eventos");

            migrationBuilder.DropColumn(
                name: "Fecha_Desde_Evento",
                table: "Eventos");

            migrationBuilder.RenameColumn(
                name: "Fecha_Hasta_Evento",
                table: "Eventos",
                newName: "Fecha_Evento");

            migrationBuilder.AddColumn<int>(
                name: "IdiomaId",
                table: "Tipos_Maquinarias",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Fecha",
                table: "Eventos",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tipos_Maquinarias_IdiomaId",
                table: "Tipos_Maquinarias",
                column: "IdiomaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tipos_Maquinarias_Idiomas_IdiomaId",
                table: "Tipos_Maquinarias",
                column: "IdiomaId",
                principalTable: "Idiomas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tipos_Maquinarias_Idiomas_IdiomaId",
                table: "Tipos_Maquinarias");

            migrationBuilder.DropIndex(
                name: "IX_Tipos_Maquinarias_IdiomaId",
                table: "Tipos_Maquinarias");

            migrationBuilder.DropColumn(
                name: "IdiomaId",
                table: "Tipos_Maquinarias");

            migrationBuilder.DropColumn(
                name: "Fecha",
                table: "Eventos");

            migrationBuilder.RenameColumn(
                name: "Fecha_Evento",
                table: "Eventos",
                newName: "Fecha_Hasta_Evento");

            migrationBuilder.AddColumn<DateTime>(
                name: "Fecha_Alta",
                table: "Eventos",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Fecha_Desde_Evento",
                table: "Eventos",
                nullable: true);
        }
    }
}
