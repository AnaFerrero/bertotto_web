﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace ClienteBB.BD.Models
{
    [Table("TiposNovedades")]
    public class TipoNovedad
    {

        public int Id { get; set; }

        public string Nombre { get; set; }

        [XmlIgnore, JsonIgnore]
        public ICollection<Novedad> Novedades { get; set; }
    }
}
