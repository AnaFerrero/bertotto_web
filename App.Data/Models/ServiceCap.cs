﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{
    public class ServiceCap
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string DescripcionCorta { get; set; }
        public string Direccion { get; set; }
        public string Provincia { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Email { get; set; }
        public string Descripcion { get; set; }
        // public DateTime? Fecha_Alta { get; set; }
        public Boolean Activo { get; set; }
        public int IdiomaId { get; set; }
        [ForeignKey("IdiomaId")]
        public Idioma Idioma { get; set; }
        public string MetaTitle { get; set; }
    }
}
