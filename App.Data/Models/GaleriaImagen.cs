﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{
    [Table("GaleriaImagenes")]
    public class GaleriaImagen
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int NoticiaId { get; set; }

        public int MaquinariaId { get; set; }
        public int RepuestoId { get; set; }

        public bool Activa { get; set; }
        public bool Principal { get; set; }
        public string Imagen { get; set; }

        public DateTime? Fecha_Alta { get; set; }
    }
}
