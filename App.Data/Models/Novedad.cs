﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{
    [Table("Novedades")]
    public class Novedad
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string DescripcionCorta { get; set; }
        public string Descripcion { get; set; }

        [NotMapped]
        public string Imagen { get; set; }
        public string MetaTitle { get; set; }

        public DateTime? Fecha_Alta { get; set; }

        public DateTime? Fecha_Desde_Notificacion { get; set; }

        public DateTime? Fecha_Hasta_Notificacion { get; set; }

        public Boolean Activo { get; set; }

        public int IdiomaId { get; set; }
        [ForeignKey("IdiomaId")]
        public Idioma Idioma { get; set; }


    }
}
