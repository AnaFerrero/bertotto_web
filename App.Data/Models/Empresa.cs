﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{

    [Table("Empresas")]
    public class Empresa
    {

        [Key]
        public int Id { get; set; }
        public string CUIT { get; set; }
        public string Nombre { get; set; }
        public string Firma { get; set; }
        public string Password { get; set; }
        //[NotMapped]
        //public string ConfirmPassword { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public DateTime? FechaAlta { get; set; }
        public string Direccion { get; set; }
        public string Numero { get; set; }
        public int LocalidadId { get; set; }
        [ForeignKey("LocalidadId")]
        public Localidad Localidad { get; set; }
        public Boolean Activo { get; set; }
    }
}
