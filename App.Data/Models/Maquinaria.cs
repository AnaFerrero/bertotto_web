﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{
    [Table("Maquinarias")]
    public class Maquinaria
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string DescripcionCorta { get; set; }
        public string Modelo { get; set; }
        public string Descripcion { get; set; }
        public string UrlFolleto { get; set; }
        public string UrlManual { get; set; }
        public string UrlPublicaManual { get; set; }
        public string MetaTitle { get; set; }
        public int? TipoId { get; set; }
        [ForeignKey("TipoId")]
        public TipoMaquinaria TipoMaquinaria { get; set; }
        public Boolean Activo { get; set; }
        public DateTime? Fecha { get; set; }
        [NotMapped]
        public string Imagen { get; set; }
        public string Estado { get; set; }
        public int? IdiomaId { get; set; }
        [ForeignKey("IdiomaId")]
        public Idioma Idioma { get; set; }
        public int MaterialId { get; set; }
        [ForeignKey("MaterialId")]
        public TipoMaterial TipoMaterial { get; set; }
    }
}
