﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace ClienteBB.BD.Models
{
    [Table("Tipos_Maquinarias")]
    public class TipoMaquinaria
    {

        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }
        public string Tipo { get; set; }

        [XmlIgnore, JsonIgnore]
        public ICollection<Maquinaria> Maquinarias { get; set; }
        public int IdiomaId { get; set; }
        [ForeignKey("IdiomaId")]
        public Idioma Idioma { get; set; }
        public int MaterialId { get; set; }
        [ForeignKey("MaterialId")]
        public TipoMaterial TipoMaterial { get; set; }

    }
}
