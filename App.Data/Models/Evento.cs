﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{
    [Table("Eventos")]
    public class Evento
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Direccion { get; set; }
        public string Fecha { get; set; }

        public DateTime? Fecha_Evento { get; set; }

        //public DateTime? Fecha_Desde_Evento { get; set; }

        //public DateTime? Fecha_Hasta_Evento { get; set; }
        public Boolean Activo { get; set; }

        public int IdiomaId { get; set; }
        [ForeignKey("IdiomaId")]
        public Idioma Idioma { get; set; }
    }
}
