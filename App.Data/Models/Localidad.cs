﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace ClienteBB.BD.Models
{
    [Table("Localidades")]
    public class Localidad
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string CodigoPostal { get; set; }
        public string Provincia { get; set; }
        [XmlIgnore, JsonIgnore]
        public ICollection<Empresa> Empresas { get; set; }
    }
}
