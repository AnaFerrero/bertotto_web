﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{
    [Table("Contactos")]
    public class Contacto
    {

        public int Id { get; set; }

        public string Asunto { get; set; }

        public string Descripcion { get; set; }

        public int UsuarioId { get; set; }

        [ForeignKey("UsuarioId")]
        public Usuario Usuario { get; set; }

        public DateTime? Fecha { get; set; }
    }
}
