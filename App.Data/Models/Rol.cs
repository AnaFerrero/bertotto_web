﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace ClienteBB.BD.Models
{
    [Table("Roles")]
    public class Rol
    {

        public int Id { get; set; }


        public string Nombre { get; set; }

        [XmlIgnore, JsonIgnore]
        public ICollection<Usuario> Usuarios { get; set; }
    }
}
