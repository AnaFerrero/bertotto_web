﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{

    [Table("Usuarios")]
    public class Usuario
    {

        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Password { get; set; }

        [NotMapped]
        public string ConfirmPassword { get; set; }

        public string Email { get; set; }

        public string Telefono { get; set; }

        public DateTime? FechaAlta { get; set; }

        public int RolId { get; set; }
        [ForeignKey("RolId")]
        public Rol Rol { get; set; }



    }
}
