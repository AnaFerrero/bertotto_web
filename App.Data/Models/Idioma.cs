﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{

    [Table("Idiomas")]
    public class Idioma
    {
        public int Id { get; set; }


        public string Nombre { get; set; }
    }
}
