﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClienteBB.BD.Models
{
    [Table("GaleriaDocumentos")]
    public class GaleriaDocumento
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int NoticiaId { get; set; }
        public string TipoDocumento { get; set; }
        public int MaquinariaId { get; set; }
        public int RepuestoId { get; set; }

        public bool Activa { get; set; }
        public bool Principal { get; set; }
        public string Documento { get; set; }

        public DateTime? Fecha_Alta { get; set; }
    }
}
