﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="politicas_calidad.aspx.cs" Inherits="ClienteBB.FrontEnd.politicas_calidad" MasterPageFile="~/Site1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner banner_politicas">
  <div class="container">
    <h2>POLITICAS DE CALIDAD</h2>
  </div>
</div>
<div class="article">
  <div class="container">
<div class="col-xs-12 empresa_item">
  <p> Todos los que integramos Bertotto S.A. trabajamos para brindar productos y servicios que satisfagan plenamente a nuestros Clientes y los requisitos aplicables, comprometiéndonos con la mejora continua y a:</p>
</div>
<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 empresa_item">
    <ul class="lista">
        <li><b style="color:#35882c;font-size:18px">1.-</b>  Garantizar diseños de equipos confiables innovadores y seguros; que aporte valor a nuestros Clientes, con menores costos y mayor productividad, obtenidos de un sistema industrial ágil y eficiente.</li>
        <li><b style="color:#35882c;font-size:18px">2.-</b>  Poner a disposición de nuestros Clientes, diversas líneas de productos afines que complementen integralmente sus necesidades.</li>
        <li><b style="color:#35882c;font-size:18px">3.-</b>  Incrementar el alcance de nuestra red comercial, que nos permita consolidar el contacto directo con nuestros Clientes para atender sus necesidades.</li>
        <li><b style="color:#35882c;font-size:18px">4.-</b>  Proporcionar un completo stock de repuestos y un servicio posventa especializado que brinde una atención eficiente.</li>
        <li><b style="color:#35882c;font-size:18px">5.-</b>  Trabajar en la formación y desarrollo de nuestros recursos humanos como impulso para la mejora continua.</li>
        </ul>
</div>

  </div>
</div>
      </asp:Content>