﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detalle_equipos.aspx.cs" Inherits="ClienteBB.FrontEnd.detalle_equipos" Title="Equipos Bertotto" MasterPageFile="~/Site1.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="/css/lightbox.min.css">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner banner_equipos">
        <div class="container">
            <h2>PRODUCTOS</h2>
        </div>
    </div>

    <div class="section blanco2">
        <div class="container">
            <div class="col-md-6">
                <div class="crop_empresa">

                    <img id="imagenEquipo" runat="server" border="0" alt="" src="">
                </div>
            </div>
            <div class="col-md-6">
                <h2 class="titulo">
                    <asp:Literal ID="litNombre" runat="server"></asp:Literal></h2>
                <p>
                    <asp:Literal ID="litDetalle" runat="server"></asp:Literal></p>


                <ul class="lista_botones">
                    <li>
                        <asp:HyperLink ID="LinkManual" runat="server" CssClass="btn btn-gris" Visible="false" Target="_blank">Manual</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="linkFolleto" runat="server" CssClass="btn btn-gris" Target="_blank">Folleto</asp:HyperLink>

                    </li>
                    <li>
                        <a class="btn btn-azul" href="#" data-toggle="modal" data-target="#exampleModal">ADQUIRIR</a>
                    </li>
                </ul>
            </div>

            <div class="col-xs-12">
                <div class="galeria_de_fotos">
                    <ul class="galeria">
                        <asp:Repeater ID="RepeatGaleria" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a class="example-image-link" href="<%# Eval("Imagen") %>" data-lightbox="example-set" data-title="">
                                        <img src="<%# Eval("Imagen") %>" alt="Equipos">
                                    </a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12" style="margin-top: 40px">
                <div class="productos_lista">
                    <h2 class="titulo2">OTROS PRODUCTOS RELACIONADOS</h2>
                    <asp:Repeater ID="RepeaterProductos" runat="server">
                        <ItemTemplate>
                            <div class="col-xs-12 col-sm-6 col-md-3 item_producto">
                                <div class="crop_producto">
                                    <img src="<%# Eval("Imagen") %>" alt="<%# Eval("Nombre") %>">
                                </div>
                                <h2><%# Eval("Nombre") %></h2>
                                <p><%# Eval("DescripcionCorta") %></p>
                                <a class="btn btn-producto" href="/Equipo/<%# Eval("MetaTitle") %>">Ver Detalle</a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="exampleModalLabel" align="center">Solicitar Información sobre el Equipo</h3>
                </div>
                <div class="modal-body">

                    <div class="form-row">
                        <p>Complete el formulario para solicitar el producto, y uno de nuestros técnicos se pondra en contacto con ud.</p>
                        <div class="form-group col-md-12">
                            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" placeholder="Nombre" required></asp:TextBox>
                        </div>
                        <div class="form-group col-md-12">
                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control" placeholder="Teléfono" required></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <asp:TextBox ID="txtMail" runat="server" CssClass="form-control" placeholder="Email" required></asp:TextBox>
                        </div>
                        <div class="form-group col-md-12">
                            <asp:TextBox ID="txtCiudad" runat="server" CssClass="form-control" placeholder="Ciudad / Provincia" required></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="name">COMENTARIOS</label>
                            <asp:TextBox ID="txtConsulta" runat="server" TextMode="MultiLine" Rows="8" CssClass="form-control" required></asp:TextBox>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnEnviar" runat="server" CssClass="btn btn-azul" Text="ENVIAR MENSAJE" OnClick="btnEnviar_Click" />
                    <br />
                    <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/lightbox-plus-jquery.min.js"></script>
</asp:Content>
