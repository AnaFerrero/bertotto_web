﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contacto.aspx.cs" Inherits="ClienteBB.FrontEnd.contacto" MasterPageFile="~/Site1.Master" %>
<%@ Register TagPrefix="asp" Namespace="GoogleRecaptchaWebForms" Assembly="GoogleRecaptchaWebForms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<div class="banner banner_contacto">
  <div class="container">
    <h2>CONTACTO</h2>
  </div>
</div>
  <div class="banda_degradado">
  </div>
<div class="article nosotros">
 <div class="container">
           <div style="padding:0px 0px;">
        <h2 class="title1">ESCRIBINOS CON TU CONSULTA</h2>
      </div>
        
    <div class="form-group ">
<asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" placeholder="Nombre y Apellido" required></asp:TextBox>
     </div>
    <div class="form-group"> 
        <asp:TextBox ID="txtMail" runat="server" CssClass="form-control" placeholder="Email" TextMode="Email" required></asp:TextBox>
   </div>
    <div class="form-group">
        <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control" placeholder="Télefono" required></asp:TextBox>
    </div>
    <div class="form-group">
        <asp:TextBox ID="txtCiudad" runat="server" CssClass="form-control" placeholder="Ciudad" required ></asp:TextBox>
    </div>
    <div class="form-group">
        <asp:TextBox ID="txtConsulta" runat="server" TextMode="MultiLine" placeholder="Tu Consulta" CssClass="form-control" Rows="6" required></asp:TextBox>
   
   </div>
    
      <div class="form-group">
            <asp:RecaptchaV2Control ID="gRecaptcha" SiteKey="6Lds6d8UAAAAAFumW5AQqFAqkIdF8H2bKHTQjlVL"
        SecretKey="6Lds6d8UAAAAAH9AagheCGSwMpADSJpT-8wZeNIX" runat="server"/>
        </div>
      
             <div class="enviar" style="margin-top:8px;display:block;"align="center">
                 <asp:Button ID="btnEnviar" runat="server" Text="Enviar Mensaje" CssClass="btn-verde" OnClick="btnEnviar_Click" />
                   <br /><asp:Label ID="lblResultado" runat="server" Text="" Font-Bold="true"></asp:Label>
             </div>
     <br />

 </div>
</div>      
    
    <script>
        function gtag_report_conversion(url) {
            var callback = function () {
                if (typeof (url) != 'undefined') {
                    window.location = url;
                }
            };
            gtag('event', 'conversion', {
                'send_to': 'AW-590938959/aOz-CPm5id4BEM-G5JkC',
                'event_callback': callback
            });
            return false;
        }
     </script>

    </asp:Content>