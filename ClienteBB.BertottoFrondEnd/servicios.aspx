﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servicios.aspx.cs" Inherits="ClienteBB.FrontEnd.servicios" MasterPageFile="~/Site1.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner banner_empresa">
  <div class="container">
    <h2>SERVICIOS</h2>
  </div>
</div>
<div class="article">

 <div class="container">
     <div class="row">
     <div class="col-md-12 empresa_item">
  <div class="col-md-6">
  <div class="parrafo">
    <p>A través de nuestro Departamento de Servicios ampliamos la oferta brindada a nuestros clientes. Esta nueva área fue proyectada y está dedicada al alquiler de sistemas de almacenamiento.
  <p>Esta modalidad está pensada para instalaciones temporarias que requieran de equipamiento para operaciones durante períodos acotados de tiempo.</p>

  <p>Algunos ejemplos son:</p>
  <p>• Obradores</p>
  <p>• Planta de Generación de Energía</p>
  <p>• Minería</p>
  <p>• Oil And Gas</p>


  <p>También puede proveer a instalaciones en formato de Comodato de Tanques con Batea, Estaciones móviles para Diesel (MOSS), para los cuales se ofrece:</p>

  <p>• Suministro de los equipos</p>
  <p>• Controles de inventario</p>
  <p>• Reportes de gestión</p>

  <p>Disponemos de una oferta de equipos de diferentes características según el uso, que nos permiten acompañar a distintos tipos de requerimientos y clientes en sus proyectos.</p>
  <p>Además de ofrecer los equipos de almacenamiento, nos encargamos del soporte técnico post entrega para asegurar el funcionamiento continuo de las instalaciones.</p>

  </div>    
  </div>
  <div class="col-md-6">
      <div class="crop_empresa">
    <img src="img/ServiciosProductos.jpg" alt="Berttoto Servicios">
  </div>
      </div>
</div>
         </div>

 </div>
</div>
    </asp:Content>