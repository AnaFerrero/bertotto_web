﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="equipos.aspx.cs" Inherits="ClienteBB.FrontEnd.equipos" MasterPageFile="~/Site1.Master" Title="Productos Bertotto Boglione" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner banner_equipos">
        <div class="container">
            <h2>PRODUCTOS</h2>
        </div>
    </div>

    <asp:ScriptManager ID="scriptmanager1" runat="server">
    </asp:ScriptManager>
    <%-- <ajax:ToolkitScriptManager runat="server">
    </ajax:ToolkitScriptManager>--%>

    <style type="text/css">
        /*body {
            font-family: Arial;
            font-size: 10pt;
        }*/

        .header_selected {
            border: 1px solid #192942;
            color: #FFF;
            background-color: #192942;
            font-weight: bold;
            padding: 5px;
            margin-top: 5px;
            cursor: pointer;
        }

        .header {
            border: 1px solid #4B7FCC;
            color: #fff;
            background-color: #4B7FCC;
            font-weight: bold;
            padding: 5px;
            margin-top: 5px;
            cursor: pointer;
        }

            .header a, .header_selected a {
                color: #fff;
                text-decoration: none;
            }

        .content {
            background-color: #F6F6F6;
        }

            .content a, .content a:visited {
                color: #000;
                margin-left: 20px;
            }

        .tabb li {
            float: left;
            margin-right: 5px;
        }

        .tabb a {
            margin: 0;
            padding: 2px 15px;
            cursor: pointer;
            text-transform: capitalize;
        }

            .tabb a:active {
                background-color: #4B7FCC;
                border-bottom: none;
                padding: 3px 10px;
            }

            .tabb a:visited {
                background-color: #4B7FCC;
                border-bottom: none;
                padding: 3px 10px;
            }
    </style>


    <asp:UpdatePanel ID="updatepnl" runat="server">
        <ContentTemplate>

            <div class=" equipos">
                <div class="container">

                    <%--  <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3" style="margin-bottom: 30px">
                            <label for="select">
                                <br>
                            </label>
                            <div class="input-group">
                            </div>
                        </div>
                    </div>--%>
                    <div class="col-xs-12 col-sm-2 col-md-4">


                        <ajax:Accordion ID="AccordionMenu" runat="server" OnItemDataBound="OnItemDataBound"
                            Width="300" HeaderCssClass="header" HeaderSelectedCssClass="header_selected"
                            ContentCssClass="content">
                            <HeaderTemplate>
                                <asp:HyperLink ID="lnkMenu" Text='<%# Eval("Nombre") %>'
                                    runat="server" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table>
                                    <asp:Repeater ID="rptMenu" runat="server" OnItemCommand="LinkButton_Command">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="tabb">
                                                    <asp:LinkButton runat="server" ID="btnSubs"
                                                        CommandName='<%# Eval("Nombre") %>'
                                                        Text='<%# Eval("Nombre") %>'
                                                        CommandArgument='<%# Eval("Id") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </ContentTemplate>
                        </ajax:Accordion>
                        <%--  <asp:Repeater ID="Repeatcateg" runat="server" OnItemDataBound="OnItemDataBound">
                                <ItemTemplate>
                                    <h1 style="background: #0a4195; color: white">
                                        <asp:Label ID="lblNombre" runat="server" Text='<%# Eval("Nombre") %>' /></h1>
                                    <div>
                                        <asp:HiddenField ID="hdMaterialId" runat="server" Value='<%# Eval("id") %>' />
                                        <asp:Repeater ID="RepeatsubCateg" runat="server">
                                            <ItemTemplate>
                                                <p>
                                                    <a class="" href="/Categoria/<%# Eval("id") %>">
                                                        <%# Eval("Nombre") %>
                                                    </a>
                                                </p>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>--%>
                    </div>


                    <div class="col-xs-12 col-sm-10 col-md-8">
                          <ul class="header" style="height=22px">
                                <asp:Repeater ID="rptBreadcrumb" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <h5><%# Eval("Nombre") %></h5>
                                         </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>


                        <asp:Repeater ID="RepeatEquipos" runat="server">
                                <ItemTemplate>
                                    <a href="/Equipo/<%# Eval("MetaTitle") %>">

                                        <div class="col-xs-12 col-sm-6 col-md-6 item_producto">
                                            <span class="linkSpanner"></span>

                                            <h2><%# Eval("Nombre") %></h2>

                                            <div class="crop_producto">

                                                <a href="/Equipo/<%# Eval("MetaTitle") %>">
                                                    <img src="<%# Eval("Imagen") %>" alt="<%# Eval("Nombre") %>">
                                                </a>

                                            </div>



                                            <%--  <p><%# Eval("DescripcionCorta") %></p>--%>
                                            <%--   <a class="btn btn-producto" href="/Equipo/<%# Eval("MetaTitle") %>">Ver Detalle</a>--%>
                                        </div>
                                    </a>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#accordion").accordion();
        });
    </script>


</asp:Content>

