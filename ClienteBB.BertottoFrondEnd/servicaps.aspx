﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servicaps.aspx.cs" Inherits="ClienteBB.FrontEnd.servicaps" MasterPageFile="~/Site1.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoZP95oeczYNwQ3egVci8Ec_IHUMDUCXI"></script>
<script type="text/javascript">

    var mapa;
    var marcas = [];
    var aLatlng = [];
    function initialize() {

        var method = 'index.aspx/getServicaps';
        var data = '';
        if (document.getElementById("<%= hdnProvincia.ClientID %>").value != "") {
            data = document.getElementById("<%= hdnProvincia.ClientID %>").value
        }
           

        var centroMapa = new google.maps.LatLng(-38.4160957, -63.6166725);
	
		var opcionesDeMapa = {
				zoom: 8,
				center: centroMapa,
				mapTypeId: google.maps.MapTypeId.ROADMAP //SATELITE, HYBRID, ROADMAP, TERRAIN
		};
		// instancia un nuevo objeto Map
        mapa = new google.maps.Map(document.getElementById("mapa"), opcionesDeMapa);		
       
		// llamada a la api para traer los servicaps filtrados por provincia
         $.ajax({
                async: true,
                type: 'POST',
                url: method,
                data: "{'provincia': '" + data + "'}",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var servicaps = jQuery.parseJSON(response.d); 
                  
                    $.each(servicaps, function (i, val) {

                        if (val.latitud != '' && val.longitud != '') {
                            aLatlng.push(new google.maps.LatLng(val.latitud, val.longitud));
                        //armar los puntos
                         marker = new google.maps.Marker({
                             position: new google.maps.LatLng(val.latitud, val.longitud),
                             icon: "/img/icono_mapa.png",
                        title: 'Servicap ' + val.nombre
                    });

                    // To add the marker to the map, call setMap();
                    marker.setMap(mapa);
                        }                       
                       
                    });

                     var latlngbounds = new google.maps.LatLngBounds();

                    for (var i = 0; i < aLatlng.length; i++) {
                        latlngbounds.extend(aLatlng[i]);
                    }
                    mapa.fitBounds(latlngbounds);
                    mapa.setZoom(8);


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Buscar Servicaps: ' + textStatus + ": " + XMLHttpRequest.responseText);
                }
        });		
	
}// acaba la función initialize
		// inicializa el mapa cuando la ventana se haya cargado:
		google.maps.event.addDomListener(window, "load", initialize);

</script>
    </asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <div class="banner banner_servicap">
  <div class="container">
    <h2>DISTRIBUIDORES</h2>
  </div>
</div>
<div class="article">
  <div class="container">
   <div class="row">
  <div class="col-xs-12 col-lg-4" style="float:right;margin-bottom:30px">
    <div class="input-group">
        <asp:DropDownList ID="ddlProvincias" runat="server" DataTextField="Nombre" DataValueField="Id" CssClass="form-control"></asp:DropDownList>
      <span class="input-group-btn">
          <asp:LinkButton ID="linkBuscar" CssClass="btn btn-default" OnClick="linkBuscar_Click" runat="server"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></asp:LinkButton>
        
      </span>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
       <div id="mapa" style="height: 650px;"></div>
        
      </div>
    </div>
  </div>
  <div class="article">
    <div class="row">
        <asp:Repeater ID="RepeatServicaps" runat="server">
    <ItemTemplate>
    <div class="col-md-4 item-servicap" style="height: 250px">
      <h3><%# Eval("Nombre") %></h3>
      <ul>
        <li><%# Eval("Direccion") %></li>
        <li><%# Eval("Descripcion") %></li>       
      </ul>
    </div>
         </ItemTemplate>
  </asp:Repeater>
    </div>
  </div>
</div>

  </div>

    <asp:HiddenField ID="hdnProvincia" runat="server" Value="" />
    </asp:Content>
