﻿using ClienteBB.FrontEnd.Class;
using System;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI;
namespace ClienteBB.FrontEnd
{
    public partial class Manual : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    var appSettings = ConfigurationManager.AppSettings;
                    string sUrlRequest = appSettings["url_api"];
                    WebClient wc = new WebClient();
                    wc.Encoding = System.Text.Encoding.UTF8;
                    var jsonResult = "";

                    if (Page.RouteData.Values["prod"] != null)
                    {

                        jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetMaquinaria/" + Page.RouteData.Values["prod"].ToString());

                        if (jsonResult != "[]")
                        {
                            JavaScriptSerializer ser = new JavaScriptSerializer();
                            Equipo maquina = ser.Deserialize<Equipo>(jsonResult);

                            Response.Redirect(maquina.UrlManual);


                        }
                    }
                }




            }
            catch (Exception ex)
            {


            }
        }
    }
}