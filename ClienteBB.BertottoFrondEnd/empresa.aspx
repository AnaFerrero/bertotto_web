﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="empresa.aspx.cs" Inherits="ClienteBB.FrontEnd.empresa" MasterPageFile="~/Site1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner banner_empresa">
        <div class="container">
            <h2>EMPRESA</h2>
        </div>
    </div>
    <div class="article">
        <div class="row">
            <div class="col-md-12 empresa_item">

                <div class="parrafo">
                    <div class="crop_empresa">
                        <img src="img/banner/bertoto-aerea.jpg" alt="Bertotto Trayectoria">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 empresa_item">
                    <div class="col-md-6">
                        <div class="parrafo">
                          
                            <p>En Bertotto-Boglione S.A. desde 1948, proveemos soluciones creativas y de altísima calidad, para resolver necesidades relativas al almacenaje de líquidos, a escala internacional.</p>

                            <p>La tecnología de última generación incorporada a los procesos productivos, el estricto cumplimiento de las normas ISO 9001 certificadas, certificación de productos UL e INMETRO y la fabricación bajo licencias mundialmente reconocidas, constituyen el cimiento de nuestra competitividad.</p>

                            <p>Tenemos vocación y compromiso internacional, logrando la obtención de registro como exportador en 1950.</p>

                            <p>Desde entonces nos esforzamos por obtener presencia en los mercados externos, beneficiados por las modernas soluciones logísticas actuales y el avance de las comunicaciones, que nos permiten ampliar nuestro horizonte hacia otros continentes.</p>

                            <p>En la búsqueda constante de la calidad nuestro trabajo se organiza en tres divisiones:</p>

                            <p>DIVISIÓN ACERO</p>
                            <p>DIVISIÓN PLÁSTICOS</p>
                            <p>DIVISIÓN ACCESORIOS</p>

                            <p>Esto permite la diversificación de nuestra producción desde tanques simples a estaciones de servicios completas de alto valor agregado y última tecnología.</p>

                            <p>Estamos especializados en la producción de tanques para depósito de combustibles de última generación, liderando la exportación, y garantizando un compromiso ambiental que asumimos junto a nuestros clientes.</p>

                            <p>
                                Siendo pioneros en su producción en América del Sur,
sumamos a la oferta exportable, "Estaciones Portátiles de Combustible".
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="parrafo">
                            <p>En la actualidad fabricamos alrededor de 2.000 tanques por año tanto en acero como en polietileno de diferentes capacidades.</p>

                            <p>Convencidos de los beneficios de crecer conforme a los estándares utilizados a nivel mundial, somos miembros del Petroleum Equipment Institute y la Association of Rotational Molders, asistiendo regularmente a sus eventos en todo el mundo.</p>

                            <p>Actualmente participamos de importantes licitaciones a nivel regional y mundial, atendiendo a empresas trasnacionales comercializadoras de combustibles, industrias químicas, alimenticias y mineras, entre muchas otras.</p>

                            <p>Contamos con un equipo conformado por técnicos que brindan los servicios de: asistencia técnica, instalación, asesoramiento y reparación, de todos los elementos que fabricamos y comercializamos. Configurando un servicio Post Venta que cierra un círculo de alta calidad en la atención y el servicio prestado a nuestros clientes.</p>

                            <p>Estamos orgullosos de nuestra historia, nuestro presente y nuestro futuro porque transitamos una camino de 67 años marcado por la coherencia en nuestra filosofía de trabajo y el crecimiento continuo, ofreciendo garantía de calidad en cada uno de nuestros productos.</p>


                            <p>COMPROMETIDOS CON EL CRECIMIENTO…</p>
                            <p>DESDE EL 1º DE MAYO DE 1948 TRABAJAMOS PARA LOGRAR EQUIPOS DE ALTO RENDIMIENTO, DONDE EL VALOR APORTADO POR LA SUMA DE SUS INTEGRANTES SEA MAYOR A LO QUE APORTAN INDIVIDUALMENTE.</p>

                            <p>CUMPLIMOS 67 AÑOS DE COMPROMISO CON EL DESARROLLO PRODUCTIVO Y LOS VALORES INDUSTRIALES….DE MARCOS JUAREZ HACIA EL MUNDO…</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h3>NUESTRA MISIÓN:</h3>
            </div>
            <div class="row">
                <div class="col-md-12 empresa_item">
                    <div class="col-md-6">
                        <div class="parrafo">
                            <p>“Aportamos soluciones creativas para el mercado Global, apoyados en el Compromiso y Satisfacción de nuestra Gente”</p>

                            <p>Nuestros Valores Fundamentales:</p>

                            <p>
                                • Honestidad: significa que no hay contradicciones ni discrepancias entre los pensamientos, palabras o acciones. Ser honesto gana la confianza de los demás. Honestidad significa nunca hacer mal uso de lo que se nos confió, es el reconocimiento de lo que está bien y es apropiado para nuestro propio papel, conducta y relaciones.
Honestidad es hablar de lo que se piensa y hacer lo que se ha dicho. Esta integración proporciona claridad y ejemplo a los demás. Su valor es visible en cada acción que realizamos.
                            </p>

                            <p>• Humildad: Actitud de la persona que no presume de sus logros, reconoce sus fracasos y debilidades, y actúa sin orgullo. La humildad implica ser auténtico y sin pretensiones ni arrogancias. Una persona humilde está abierta al aprendizaje.</p>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="parrafo">
                            <p>• Compromiso: Actitud positiva y responsable por parte de una persona para el logro de los objetivos, fines y metas de la organización o equipo del que forma parte, en el cual cada miembro aporta su máxima capacidad con gran sentido de pertenencia.</p>

                            <p>• Responsabilidad: Una persona responsable toma decisiones conscientemente y acepta las consecuencias de sus actos, dispuesto a rendir cuenta de ellos. La responsabilidad es la virtud o disposición habitual de asumir las consecuencias de las propias decisiones, respondiendo de ellas ante alguien. Responsabilidad es la capacidad de dar respuesta de los propios actos.</p>

                            <p>• Integridad: consiste en la integración de ideales, convicciones, normas, creencias, por una parte y la conducta, por otra. Cuando nuestra conducta es congruente con nuestros valores declarados, cuando concuerdan los ideales y la práctica, tenemos integridad.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h3>POLÍTICA DE CALIDAD</h3>
            </div>
            <div class="row">
                <div class="col-md-12 empresa_item">

                    <div class="parrafo">
                        <p>Todos los que integramos Bertotto Boglione S.A., trabajamos con el objeto de brindar productos y servicios que satisfagan plenamente a nuestros Clientes cumpliendo con los requisitos aplicables, comprometiéndonos a:</p>

                        <p>1. Garantizar la salud y seguridad de todas las personas que desempeñan tareas en la organización.</p>

                        <p>2. Diseñar equipos confiables, innovadores y seguros; que aporten valor a nuestros Clientes en el almacenamiento de sólidos y fluidos para el mercado global.</p>

                        <p>3. Producir bajo un sistema industrial flexible con el objeto de reducir costos e incrementar la eficiencia.</p>

                        <p>4. Proporcionar un completo stock de accesorios y un servicio técnico especializado que nos permita consolidar el contacto directo con nuestros Clientes.</p>

                        <p>5. Trabajar en la formación y desarrollo de nuestros recursos humanos como impulso para la mejora continua.</p>

                        <p>6. Promover los valores que representan a la organización: Honestidad, Humildad, Compromiso, Responsabilidad e Integridad.</p>

                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
