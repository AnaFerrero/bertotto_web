﻿using System;
using System.Web.Routing;

namespace ClienteBB.FrontEnd
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterCustomRoutes(RouteTable.Routes);
        }
        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("X-Frame-Options");
            Response.AddHeader("X-Frame-Options", "AllowAll");

        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }


        void RegisterCustomRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("home", "Home", "~/index.aspx");
            routes.MapPageRoute("cms", "admin", "~/login.aspx");
            routes.MapPageRoute("politicas", "Politicas-calidad", "~/politicas_calidad.aspx");
            routes.MapPageRoute("novedades", "Novedades", "~/novedades.aspx");
            routes.MapPageRoute("novedad", "Noticia/{not}", "~/detalle_noticia.aspx");
            routes.MapPageRoute("nosotros", "Empresa", "~/empresa.aspx");
            routes.MapPageRoute("productos", "Equipos", "~/equipos.aspx");
            routes.MapPageRoute("producto", "Equipo/{prod}", "~/detalle_equipos.aspx");
            routes.MapPageRoute("productosestados", "Equipos/{estado}", "~/equipos.aspx");
            routes.MapPageRoute("categoria", "Categoria/{cat}", "~/equipos.aspx");
            routes.MapPageRoute("contacto", "Contacto", "~/contacto.aspx");
            routes.MapPageRoute("servicap", "Servicap", "~/servicaps.aspx");
            routes.MapPageRoute("confirmacion", "Confirmacion-Consulta", "~/confirm_producto.aspx");
            routes.MapPageRoute("manual", "Manual/{prod}", "~/Manual.aspx");

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}