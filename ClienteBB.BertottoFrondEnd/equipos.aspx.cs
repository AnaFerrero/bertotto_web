﻿using AjaxControlToolkit;
using ClienteBB.FrontEnd.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClienteBB.FrontEnd
{
    public partial class equipos : System.Web.UI.Page
    {

        protected void OnItemDataBound(object sender, AccordionItemEventArgs e)
        {
            if (e.ItemType == AccordionItemType.Header)
            {
                string menuText = (e.AccordionItem.FindControl("lnkMenu") as HyperLink).Text;
                //if (menuText == "1")
                //{
                AccordionMenu.SelectedIndex = e.ItemIndex;
                // }
            }
            if (e.ItemType == AccordionItemType.Content)
            {
                WebClient wc = new WebClient();
                wc.Encoding = System.Text.Encoding.UTF8;
                var jsonResult = "";

                var appSettings = ConfigurationManager.AppSettings;
                string sUrlRequest = appSettings["url_api"];


                //categorias
                var category = (Categoria)e.Item; 
                jsonResult = wc.DownloadString(sUrlRequest + "/TipoMaquinaria/GetByMaterial/" + category.Id);

                if (jsonResult != "[]")
                {
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    List<Categoria> categ = ser.Deserialize<List<Categoria>>(jsonResult);

                    AccordionContentPanel cPanel = e.AccordionItem;
                    Repeater rptMenu = (Repeater)cPanel.FindControl("rptMenu");
                    rptMenu.DataSource = categ;
                    rptMenu.DataBind();

                }

            }
        }

        //protected void rpt1_ItemCommand2(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        //{

        //    LinkButton linkbtn = (LinkButton)e.Item.FindControl("btnSubs");
        //    linkbtn.ForeColor = System.Drawing.Color.Red;
        //    GetMaquinarias("1");
        //    //  my(code);
        //}

        protected void LinkButton_Command(object source, RepeaterCommandEventArgs e)
        {
            GetMaquinarias(e.CommandArgument.ToString(), e.CommandName.ToString());
            //LinkButton linkbtn = (LinkButton)e.Item.FindControl("btnSubs");
            //linkbtn.ForeColor = System.Drawing.Color.blu;
        }

        private void GetMaquinarias(string category, string name)
        {
            //por categorias
            var appSettings = ConfigurationManager.AppSettings;
            string sUrlRequest = appSettings["url_api"];

            WebClient wc = new WebClient();
            wc.Encoding = System.Text.Encoding.UTF8;
            var jsonResult = "";

            JavaScriptSerializer ser = new JavaScriptSerializer();

            RepeatEquipos.DataSource = null;
            RepeatEquipos.DataBind();


            jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetActiveByTipo/" + category + "/All");

            if (jsonResult != "[]")
            {
                List<Equipo> equipos = ser.Deserialize<List<Equipo>>(jsonResult);
                RepeatEquipos.DataSource = equipos;
                RepeatEquipos.DataBind();

            }

            List<Categoria> list = new List<Categoria>();
            Categoria filter = new Categoria();
            filter.Nombre = name;
            filter.Id =Convert.ToInt32(category);
            list.Add(filter);
            rptBreadcrumb.DataSource = list;
            rptBreadcrumb.DataBind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                if (!IsPostBack)
                {


                    var appSettings = ConfigurationManager.AppSettings;
                    string sUrlRequest = appSettings["url_api"];

                    WebClient wc = new WebClient();
                    wc.Encoding = System.Text.Encoding.UTF8;
                    var jsonResult = "";

                    //categorias
                    jsonResult = wc.DownloadString(sUrlRequest + "/TipoMaterial/GetActiveAll");

                    if (jsonResult != "[]")
                    {
                        JavaScriptSerializer ser = new JavaScriptSerializer();
                        List<Categoria> categ = ser.Deserialize<List<Categoria>>(jsonResult);

                        AccordionMenu.DataSource = categ;
                        AccordionMenu.DataBind();

                    }

                    if (Page.RouteData.Values["estado"] != null)
                    {
                        //filtrar por productos nuevos o usados
                        var estado = Page.RouteData.Values["estado"].ToString();

                        if (estado == "Nuevos")
                            jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetActiveByEstado/Nueva");
                        else
                            jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetActiveByEstado/Usada");

                        if (jsonResult != "[]")
                        {
                            JavaScriptSerializer ser = new JavaScriptSerializer();
                            List<Equipo> equipos = ser.Deserialize<List<Equipo>>(jsonResult);

                            RepeatEquipos.DataSource = equipos;
                            RepeatEquipos.DataBind();

                        }
                    }
                    else
                    {
                        if (Page.RouteData.Values["cat"] != null)
                        {
                            //por categorias
                            JavaScriptSerializer ser = new JavaScriptSerializer();


                            jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetActiveByTipo/" + Page.RouteData.Values["cat"].ToString() + "/All");

                            if (jsonResult != "[]")
                            {

                                List<Equipo> equipos = ser.Deserialize<List<Equipo>>(jsonResult);

                                RepeatEquipos.DataSource = equipos;
                                RepeatEquipos.DataBind();


                            }


                            jsonResult = wc.DownloadString(sUrlRequest + "/TipoMaquinaria/GetById/" + Page.RouteData.Values["cat"].ToString());
                            if (jsonResult != "[]")
                            {
                                Categoria categorias = ser.Deserialize<Categoria>(jsonResult);

                                List<Categoria> list = new List<Categoria>();
                                //list.Add(categorias);
                                //rptBreadcrumb.DataSource = list;
                                //rptBreadcrumb.DataBind();

                                AccordionMenu.DataSource = list;
                                AccordionMenu.DataBind();
                            }

                        }
                        else
                        {

                            //todos
                            jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetActiveAll");

                            if (jsonResult != "[]")
                            {
                                JavaScriptSerializer ser = new JavaScriptSerializer();
                                List<Equipo> equipos = ser.Deserialize<List<Equipo>>(jsonResult);

                                RepeatEquipos.DataSource = equipos;
                                RepeatEquipos.DataBind();

                            }
                        }
                    }

                }


            }
            catch (Exception ex)
            {

            }
        }

        protected void OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    string materialId = (e.Item.FindControl("hdMaterialId") as HiddenField).Value;
                    Repeater RepeatsubCateg = e.Item.FindControl("RepeatsubCateg") as Repeater;



                    WebClient wc = new WebClient();
                    wc.Encoding = System.Text.Encoding.UTF8;
                    var jsonResult = "";

                    var appSettings = ConfigurationManager.AppSettings;
                    string sUrlRequest = appSettings["url_api"];


                    //categorias
                    jsonResult = wc.DownloadString(sUrlRequest + "/TipoMaquinaria/GetByMaterial/" + materialId);

                    if (jsonResult != "[]")
                    {
                        JavaScriptSerializer ser = new JavaScriptSerializer();
                        List<Categoria> categ = ser.Deserialize<List<Categoria>>(jsonResult);

                        RepeatsubCateg.DataSource = categ;
                        RepeatsubCateg.DataBind();

                    }



                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}