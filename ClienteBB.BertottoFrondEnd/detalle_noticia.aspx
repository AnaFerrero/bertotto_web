﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detalle_noticia.aspx.cs" Inherits="ClienteBB.FrontEnd.detalle_noticia" MasterPageFile="~/Site1.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
 <link rel="stylesheet" href="/css/lightbox.min.css">
    </asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner banner_equipos">
  <div class="container">
    <h2>DETALLE NOTICIA</h2>
  </div>
</div>

<div class="section blanco2">
    <div class="container">
       
            <h1 class="title1">
              <asp:Label ID="lblTitulo" runat="server" Text="Label"></asp:Label>
            </h1>
        
        <div class="crop_1">
            <img id="imgNoticia" runat="server" border="0" alt="" src="" />
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12" style="margin:40px 0px">
            <p> <asp:Label ID="lblDetalle" runat="server" Text="Label"></asp:Label></p>
            
        </div>
        <div class="col-xs-12">
            <div class="galeria_de_fotos">
                <ul class="galeria">
                    <asp:Repeater ID="RepeatGaleria" runat="server">
                    <ItemTemplate>
                    <li>
                        <a class="example-image-link" href="<%# Eval("Imagen") %>" data-lightbox="example-set" data-title="">
                            <img src="<%# Eval("Imagen") %>" alt="Noticas">
                        </a>
                    </li>
                   </ItemTemplate>
                    </asp:Repeater> 
                </ul>
            </div>
        </div>
    </div>
</div>
    <script src="/js/lightbox-plus-jquery.min.js"></script>
    </asp:Content>
