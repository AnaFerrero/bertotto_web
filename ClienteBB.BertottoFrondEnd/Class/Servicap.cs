﻿namespace ClienteBB.FrontEnd.Class
{
    public class Servicap
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string DescripcionCorta { get; set; }
        public string Direccion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Email { get; set; }
        public string Descripcion { get; set; }
    }
}