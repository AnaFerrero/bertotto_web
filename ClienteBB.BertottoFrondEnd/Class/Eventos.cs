﻿namespace ClienteBB.FrontEnd.Class
{
    public class Eventos
    {

        public int Id { get; set; }

        public string Nombre { get; set; }

        public string Direccion { get; set; }
        public string Fecha { get; set; }
        public string Dia { get; set; }
        public string Mes { get; set; }
    }
}