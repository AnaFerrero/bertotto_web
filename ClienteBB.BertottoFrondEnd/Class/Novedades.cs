﻿namespace ClienteBB.FrontEnd.Class
{
    public class Novedades
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string DescripcionCorta { get; set; }
        public string Descripcion { get; set; }
        public string Imagen { get; set; }
        public string MetaTitle { get; set; }
    }
}