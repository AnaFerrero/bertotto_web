﻿using System;
using System.Collections.Generic;

namespace ClienteBB.FrontEnd.Class
{
    public class Equipo
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string DescripcionCorta { get; set; }
        public string Modelo { get; set; }
        public string Descripcion { get; set; }
        public string TipoMaquina { get; set; }

        public string UrlFolleto { get; set; }
        public string UrlManual { get; set; }
        public string MetaTitle { get; set; }
        public Boolean Activo { get; set; }
        public DateTime? Fecha { get; set; }
        public string Imagen { get; set; }

        public List<Galeria> Imagenes { get; set; }
    }
}