﻿using ClienteBB.FrontEnd.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace ClienteBB.FrontEnd
{
    public partial class ManualUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    var appSettings = ConfigurationManager.AppSettings;
                    string sUrlRequest = appSettings["url_api"];
                    WebClient wc = new WebClient();
                    wc.Encoding = System.Text.Encoding.UTF8;
                    var jsonResult = "";

                    if (Page.RouteData.Values["prod"] != null)
                    {

                        jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetMaquinaria/" + Page.RouteData.Values["prod"].ToString());

                        if (jsonResult != "[]")
                        {
                            JavaScriptSerializer ser = new JavaScriptSerializer();
                            Equipo maquina = ser.Deserialize<Equipo>(jsonResult);

                            Response.Redirect(maquina.UrlManual);
                            //((HtmlControl)(this.FindControl("myIframe"))).Attributes["src"] = maquina.UrlManual;
                            //  iframeDiv.Controls.Add(new LiteralControl("<iframe src=\"" + maquina.UrlManual + "&embedded=true\" ></iframe><br />"));

                        }
                    }
                }




            }
            catch (Exception ex)
            {


            }
        }

      
    }
}