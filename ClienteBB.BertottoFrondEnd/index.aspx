﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="ClienteBB.FrontEnd.index" MasterPageFile="~/Site1.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner_movil visible-xs">
        <div class="container">
            <div class="col-xs-12">
                <h1 class="caption_titlem" style="text-align: left"></h1>
                <p class="caption_textm">
                </p>
                <a class="caption_btn" href="/Equipos">VER MÁS</a>
            </div>
        </div>
    </div>
    <div class="slider-wrapper theme-mi-slider sombra visible-md visible-sm visible-lg" style="z-index: 1">
        <div id="slider" class="nivoSlider">
            <img src="img/slider/banner_1.jpg" alt="AdBlue" title="#htmlcaption1" />
            <img src="img/slider/banner_2.jpg" alt="Comedero" title="#htmlcaption2" />
            <img src="img/slider/banner_3.jpg" alt="Berttoto" title="#htmlcaption3" />
          <img src="img/slider/banner_4.jpg" alt="Berttoto" title="#htmlcaption4" />
        </div>
        <div id="htmlcaption1" class="nivo-html-caption">
            <div class="container">
                <div class="col-xs-6">
                    <h1 class="caption_title1"></h1>
                    <p class="caption_text"></p>
                    <div style="margin-top: 20px"></div>
                </div>
            </div>
        </div>
        <div id="htmlcaption2" class="nivo-html-caption">
            <div class="container">
                <div class="col-xs-6" style="padding: 0px">
                    <h1 class="caption_title1"></h1>
                    <p class="caption_text"></p>
                    <div style="margin-top: 20px"></div>
                </div>
            </div>
        </div>
        <div id="htmlcaption3" class="nivo-html-caption">
            <div class="container">
                <div class="col-xs-6" style="padding: 0px">
                    <h1 class="caption_title1"></h1>
                    <p class="caption_text"></p>
                    <div style="margin-top: 20px"></div>
                </div>
            </div>
        </div>
          <div id="htmlcaption4" class="nivo-html-caption">
            <div class="container">
                <div class="col-xs-6" style="padding: 0px">
                    <h1 class="caption_title1"></h1>
                    <p class="caption_text"></p>
                    <div style="margin-top: 20px"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="article" id="anclaempresa">
        <div class="container">
            <h1 class="title1 visible-md visible-sm visible-lg">CONTENEMOS VALOR</h1>
            <p align="center" style="font-size: 22px; color: #555">Todos los que integramos <strong>Bertotto Boglione S.A.</strong>, Aportamos soluciones creativas para el mercado Global, apoyados en el Compromiso y Satisfacción de nuestra Gente"</p>
            <div class="row" style="padding: 40px 0px">
                <div class="col-xs-12 col-sm-6 col-md-3 item_nosotros" align="center">
                    <a href="/Empresa" style="text-decoration: none;">
                        <img src="img/trayectoria.png" alt="Tecnología Tanques Rotomoldeado">
                        <h3>Trayectoria</h3>
                    </a>
                    <div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 item_nosotros" align="center">
                    <a href="/Empresa" style="text-decoration: none;">
                        <img src="img/solidez.png" alt="Tanques Rotomoldeado Plastico">
                        <h3>Honestidad</h3>
                    </a>
                    <div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 item_nosotros" align="center">
                    <a href="/Empresa" style="text-decoration: none;">
                        <img src="img/calidad.png" alt="Tanques Rotomoldeado Acero">
                        <h3>Humildad</h3>
                    </a>
                    <div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 item_nosotros" align="center">
                    <a href="/Servicap">
                        <img src="img/cerca_tuyo.png" alt="Calidad">
                        <h3>Responsabilidad</h3>
                    </a>
                    <div>
                    </div>
                </div>
            </div>
        </div>
        <div id="anclaequipos">
        </div>
    </div>

     <div class="container">
        <div class="col-md-12 text-center">
            <h4>  <a class="btn-verde"  href="https://cintelink.com.ar/bertotto/">Control de Flotas Online</a> </h4>
           
        </div>
    </div>


    <div class="article">
        <div class="container-fluid" style="padding: 0px">
            <div class="col-md-3">
                <a href="/Equipos/Nuevos" style="text-decoration: none;">

                    <img src="img/accesorios_img.jpg" alt="Accesorios">
                </a>
            </div>
            <div class="col-md-3">
                <a href="/Equipos/Usados" style="text-decoration: none;">
                    <img src="img/acero_index.jpg" alt="Acero"></a>
            </div>

            <div class="col-md-3">

                <a href="#" style="text-decoration: none;">

                    <img src="img/gasandoil_img.jpg" alt="Olio y Gas"></a>
            </div>
            <div class="col-md-3">

                <a href="#" style="text-decoration: none;">
                    <img src="img/plastico_index.jpg" alt="Plastico"></a>
            </div>


        </div>
        <%--<div class="container-fluid">

            <div class="col-md-3 col-sm-4 col-md-12">
                <%-- <img src="/img/logo.png" alt="" style="width:200px">
                <a class="btn-borde btn-verde" href="https://cintelink.com.ar/bertotto/">Control de Flota Online</a>
            </div>
        </div>--%>
    </div>

   

    <div id="anclamapa">
    </div>


    <%--<div class="mapa">
<div class="container-fluid">
    <div class="col-xs-12 col-sm-6 col-md-4 donde">
    <h2>¿DÓNDE ENCONTRARNOS?</h2>
    <a class="btn-verde" href="/Servicap">Buscar Distribuidoress</a>
  </div>
</div>
</div>--%>
    <div class="article" id="anclanovedades">
        <div class="container">
            <div class="owl-carousel owl-theme">
                <asp:Repeater ID="RepeatNovedades" runat="server">
                    <ItemTemplate>
                        <div class="owl-item">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="crop_img_carousel">
                                    <img src="<%# Eval("Imagen") %>" alt="<%# Eval("Nombre") %>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6" style="margin-top: 5%">
                                <h2 class="titulo_novedades"><%# Eval("Nombre") %></h2>
                                <p style="margin-bottom: 30px"><%# Eval("DescripcionCorta") %></p>
                                <a href="/Noticia/<%# Eval("MetaTitle") %>" class="btn-verde">Ver más</a>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>


    <div class="contactarnos">
        <div class="container">
            <div class="titulo_footer">
               <%-- <h1>CUMPLIMOS 71 AÑOS DE COMPROMISO CON EL DESARROLLO PRODUCTIVO Y LOS VALORES INDUSTRIALES.</h1>--%>
                <a class="btn_borde" href="/Contacto">CONTACTANOS</a>
            </div>
        </div>
    </div>
    <div id="anclafooter">
    </div>
</asp:Content>
