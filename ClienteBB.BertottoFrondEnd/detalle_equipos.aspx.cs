﻿using ClienteBB.FrontEnd.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI;

namespace ClienteBB.FrontEnd
{
    public partial class detalle_equipos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    var appSettings = ConfigurationManager.AppSettings;
                    string sUrlRequest = appSettings["url_api"];
                    WebClient wc = new WebClient();
                    wc.Encoding = System.Text.Encoding.UTF8;
                    var jsonResult = "";

                    if (Page.RouteData.Values["prod"] != null)
                    {

                        jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetMaquinaria/" + Page.RouteData.Values["prod"].ToString());

                        if (jsonResult != "[]")
                        {
                            JavaScriptSerializer ser = new JavaScriptSerializer();
                            Equipo maquina = ser.Deserialize<Equipo>(jsonResult);

                            litNombre.Text = maquina.TipoMaquina + " " + maquina.Nombre;
                            litDetalle.Text = maquina.Descripcion;
                            imagenEquipo.Src = maquina.Imagen;
                            imagenEquipo.Alt = maquina.Nombre;
                            linkFolleto.NavigateUrl = maquina.UrlFolleto;
                            LinkManual.NavigateUrl = maquina.UrlManual;

                            //busco la galeria de imagenes
                            RepeatGaleria.DataSource = maquina.Imagenes;
                            RepeatGaleria.DataBind();

                            //muestro 4 productos relacionados

                            jsonResult = wc.DownloadString(sUrlRequest + "/Maquinaria/GetMaquinariaRelacionada/" + maquina.Id);

                            if (jsonResult != "[]")
                            {
                                ser = new JavaScriptSerializer();
                                List<Equipo> maquinaRelacionada = ser.Deserialize<List<Equipo>>(jsonResult);

                                RepeaterProductos.DataSource = maquinaRelacionada;
                                RepeaterProductos.DataBind();
                            }
                        }
                    }
                }




            }
            catch (Exception ex)
            {


            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                //enviar mail para consultas de maquinarias
                var appSettings = ConfigurationManager.AppSettings;
                string mail_to = appSettings["mail_to"];
                string mail_from = appSettings["mail_from"];

                //Creamos el contenido del mensaje
                string Mensaje = "Se recibio una consulta por el Producto " + litNombre.Text + "<br />";
                Mensaje += "Contacte al interesado para completar el proceso <br /><br />";
                Mensaje += "<br />Nombre: " + txtNombre.Text.Trim()
           + "<br /><br />Télefono: " + txtTelefono.Text
               + "<br /><br />E-Mail: " + txtMail.Text.Trim()
                   + "<br /><br />Ciudad: " + txtCiudad.Text.Trim()
           + "<br /><br />CONSULTA: " + txtConsulta.Text;

                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

                mailMessage.From = new System.Net.Mail.MailAddress(mail_from, "Bertotto Web");
                mailMessage.To.Add(new System.Net.Mail.MailAddress(mail_to));

                //Set the subjet and body text
                mailMessage.Subject = "Bertotto consulta web por un producto.";
                mailMessage.Body = Mensaje;
                //Text/HTML
                mailMessage.IsBodyHtml = true;
                mailMessage.Priority = System.Net.Mail.MailPriority.Normal;

                //Create an instance of the SmtpClient class for sending the email              

                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();

                try
                {
                    //Create an instance of the SmtpClient class for sending the email

                    smtpClient.Send(mailMessage);
                    Response.Redirect("/Confirmacion-Consulta", true);
                }
                catch (System.Net.Mail.SmtpException smtpExc)
                {
                    lblResult.Text = "Se produjo un error intente más tarde.  " + smtpExc.ToString();
                }
                catch (Exception ex)
                {
                    lblResult.Text = "Se produjo un error intente más tarde. " + ex.ToString();
                }
            }
            catch (Exception ex)
            {


            }
        }
    }
}