﻿using System;
using System.Configuration;

namespace ClienteBB.FrontEnd
{
    public partial class contacto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                var result = gRecaptcha.IsValid;
                // var result = true;
                if (result)
                {
                    var appSettings = ConfigurationManager.AppSettings;
                    string mail_to = appSettings["mail_to"];
                    string mail_from = appSettings["mail_from"];

                    //Creamos el contenido del mensaje
                    string Mensaje = "Nuevo Consulta desde la Pagina web de Bertotto <br /><br /><br />Nombre: " + txtNombre.Text.Trim()
                        + "<br /><br />Télefono: " + txtTelefono.Text
                        + "<br /><br />E-Mail: " + txtMail.Text.Trim()
                            + "<br /><br />Ciudad: " + txtCiudad.Text.Trim()
                    + "<br /><br />CONSULTA: " + txtConsulta.Text;



                    System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

                    mailMessage.From = new System.Net.Mail.MailAddress(mail_from, "Bertotto Web");
                    mailMessage.To.Add(new System.Net.Mail.MailAddress(mail_to));

                    //Set the subjet and body text
                    mailMessage.Subject = "Bertotto Argentina - Consulta desde sitio web.";
                    mailMessage.Body = Mensaje;
                    //Text/HTML
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Priority = System.Net.Mail.MailPriority.Normal;

                    //Create an instance of the SmtpClient class for sending the email              

                    System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();

                    try
                    {
                        //Create an instance of the SmtpClient class for sending the email

                        smtpClient.Send(mailMessage);
                        lblResultado.Text = "Recibimos su consulta, le responderemos a la brevedad. Gracias por contactarnos.";
                    }
                    catch (System.Net.Mail.SmtpException smtpExc)
                    {
                        lblResultado.Text = "Se produjo un error intente más tarde.  " + smtpExc.ToString();
                    }
                    catch (Exception ex)
                    {
                        lblResultado.Text = "Se produjo un error intente más tarde. " + ex.ToString();
                    }
                }
                else
                {
                    lblResultado.ForeColor = System.Drawing.Color.Red;
                    lblResultado.Text = "El código de verificación es incorrecto.";
                }
            }
            catch (Exception ex)
            {


            }
        }
    }
}