﻿using ClienteBB.FrontEnd.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI;

namespace ClienteBB.FrontEnd
{
    public partial class detalle_noticia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string sUrlRequest = appSettings["url_api"];

                if (Page.RouteData.Values["not"] != null)
                {
                    //get data novedades
                    WebClient wc = new WebClient();
                    wc.Encoding = System.Text.Encoding.UTF8;
                    var jsonNovedad = wc.DownloadString(sUrlRequest + "/Novedad/GetNovedad/" + Page.RouteData.Values["not"].ToString());

                    if (jsonNovedad != "[]")
                    {
                        JavaScriptSerializer ser = new JavaScriptSerializer();
                        Novedades novedad = ser.Deserialize<Novedades>(jsonNovedad);

                        lblTitulo.Text = novedad.Nombre;
                        lblDetalle.Text = novedad.Descripcion;
                        imgNoticia.Src = novedad.Imagen;
                        imgNoticia.Alt = novedad.Nombre;

                        //busco la galeria de imagenes
                        var jsonGaleria = new WebClient().DownloadString(sUrlRequest + "/Novedad/GetImagenes/" + novedad.Id);
                        if (jsonGaleria != "[]")
                        {

                            List<Galeria> galeria = ser.Deserialize<List<Galeria>>(jsonGaleria);
                            RepeatGaleria.DataSource = galeria;
                            RepeatGaleria.DataBind();
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                // throw;
            }
        }
    }
}