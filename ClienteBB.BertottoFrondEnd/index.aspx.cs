﻿using ClienteBB.FrontEnd.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace ClienteBB.FrontEnd
{
    public partial class index : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string sUrlRequest = appSettings["url_api"];

                WebClient wc = new WebClient();
                wc.Encoding = System.Text.Encoding.UTF8;

                //get data novedades
                var jsonNovedades = wc.DownloadString(sUrlRequest + "/Novedad/GetActiveAll");

                if (jsonNovedades != "[]")
                {
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    List<Novedades> novedades = ser.Deserialize<List<Novedades>>(jsonNovedades);

                    RepeatNovedades.DataSource = novedades;
                    RepeatNovedades.DataBind();
                }

                //get data eventos

                /* var jsonEventos = wc.DownloadString(sUrlRequest + "/Evento/GetActiveAll");

                 if(jsonEventos != "[]")
                 {
                     JavaScriptSerializer ser = new JavaScriptSerializer();
                     List<Eventos> eventos = ser.Deserialize<List<Eventos>>(jsonEventos);

                     foreach (var item in eventos)
                     {
                         var fecha = item.Fecha.Split(' ');
                         item.Dia = fecha[0];
                         item.Mes = fecha.Length >= 3 ? fecha[2].Substring(0, 3).ToUpper() : fecha[1].Substring(0, 3).ToUpper();

                     }
                     RepeaterEventos.DataSource = eventos;
                     RepeaterEventos.DataBind();
                 }*/

            }
            catch (Exception ex)
            {


            }

        }

        [WebMethod]
        public static string getServicaps(string provincia)
        {
            var appSettings = ConfigurationManager.AppSettings;
            string sUrlRequest = appSettings["url_api"];

            WebClient wc = new WebClient();
            wc.Encoding = System.Text.Encoding.UTF8;
            if (provincia == "")
                return wc.DownloadString(sUrlRequest + "/ServiceCap/GetActiveAll/");
            else
                return wc.DownloadString(sUrlRequest + "/ServiceCap/GetActiveByProvincia/" + provincia);
        }


    }
}