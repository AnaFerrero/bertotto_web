﻿using ClienteBB.FrontEnd.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;

namespace ClienteBB.FrontEnd
{
    public partial class servicaps : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {


                    var appSettings = ConfigurationManager.AppSettings;
                    string sUrlRequest = appSettings["url_api"];

                    WebClient wc = new WebClient();
                    wc.Encoding = System.Text.Encoding.UTF8;
                    var jsonResult = wc.DownloadString(sUrlRequest + "/ServiceCap/GetActiveAll/");

                    if (jsonResult != "[]")
                    {
                        JavaScriptSerializer ser = new JavaScriptSerializer();
                        List<Servicap> servicaps = ser.Deserialize<List<Servicap>>(jsonResult);

                        RepeatServicaps.DataSource = servicaps;
                        RepeatServicaps.DataBind();

                    }

                    //provincias

                    jsonResult = wc.DownloadString(sUrlRequest + "/ServiceCap/GetProvincias");

                    if (jsonResult != "[]")
                    {
                        JavaScriptSerializer ser = new JavaScriptSerializer();
                        List<Provincia> provincias = ser.Deserialize<List<Provincia>>(jsonResult);

                        ddlProvincias.DataSource = provincias;
                        ddlProvincias.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {

            }


        }

        protected void linkBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string sUrlRequest = appSettings["url_api"];

                WebClient wc = new WebClient();
                wc.Encoding = System.Text.Encoding.UTF8;
                var jsonServicaps = wc.DownloadString(sUrlRequest + "/ServiceCap/GetActiveByProvincia/" + ddlProvincias.SelectedValue);

                if (jsonServicaps != "[]")
                {
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    List<Servicap> servicaps = ser.Deserialize<List<Servicap>>(jsonServicaps);

                    RepeatServicaps.DataSource = servicaps;
                    RepeatServicaps.DataBind();

                }
                hdnProvincia.Value = ddlProvincias.SelectedValue;
                //recargar la info del mapa 
                ClientScript.RegisterClientScriptBlock(GetType(), "sas", " initialize();", true);

            }
            catch (Exception ex)
            {


            }
        }
    }
}