﻿using ClienteBB.BD.DAL;
using ClienteBB.Web.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace ClienteBB.Web.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class ContactoController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public ContactoController(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Contacto/SendEmail")]
        public async Task<IActionResult> SendEmail(int id, string asunto, string mensaje)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var usuario = await DB.Usuarios.Where(x => x.Id == id).FirstOrDefaultAsync();

                var Email = _configuration.GetSection("Email").Value;

                if (usuario != null)
                {

                    #region Contacto
                    BD.Models.Contacto _contacto = new BD.Models.Contacto
                    {
                        Asunto = asunto,
                        Descripcion = mensaje,
                        Fecha = DateTime.Now,
                        UsuarioId = usuario.Id
                    };
                    DB.Contactos.Add(_contacto);
                    await DB.SaveChangesAsync();
                    #endregion

                    var KEY = _configuration.GetSection("API_Key").Value;
                    var client = new SendGridClient(KEY);
                    var from = new EmailAddress(usuario.Email, usuario.Nombre);
                    List<EmailAddress> tos = new List<EmailAddress>
                    {
                        new EmailAddress("aferrero@dlconsultores.com.ar", "Example User 2"),
                        new EmailAddress(Email, "ClienteBB")
                    };
                    //consultas@ClienteBB.com.ar
                    var subject = asunto;
                    var htmlContent = mensaje;

                    var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, "", htmlContent);
                    var response = await client.SendEmailAsync(msg);


                    if (response.StatusCode != HttpStatusCode.Accepted)
                    {
                        return BadRequest(new { message = response.StatusCode });
                    }

                    return Ok(new { message = "Mensaje enviado con éxito." });
                }
                else
                {
                    return BadRequest(new { message = "Usuario no existe." });
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }


        [HttpGet]
        [Route("api/Contacto/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(await DB.Contactos.Include(x => x.Usuario).OrderByDescending(x => x.Fecha).ToListAsync());
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [HttpGet]
        [Route("api/Contacto/DownloadFile")]
        public IActionResult DownloadFile()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {


                string webRootPath = _hostingEnvironment.WebRootPath;
                string contentRootPath = _hostingEnvironment.ContentRootPath;

                FileInfo file = new FileInfo(webRootPath + "\\templates\\template.xlsx");
                string newfileName = webRootPath + @"\\templates\\REPORT_" + DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss") + ".xlsx";

                using (ExcelPackage package = new ExcelPackage(file))
                {

                    var items = DB.Contactos.Include(x => x.Usuario).OrderBy(x => x.Fecha).ToList();

                    ExcelWorkbook excelWorkBook = package.Workbook;
                    ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets[1];


                    for (int c = 4; c < items.Count() + 4; c++)
                    {
                        excelWorksheet.Cells[c, 1].Value = items[c - 4].Fecha;
                        excelWorksheet.Cells[c, 2].Value = items[c - 4].Usuario.Nombre;
                        excelWorksheet.Cells[c, 3].Value = items[c - 4].Usuario.Email;
                        excelWorksheet.Cells[c, 4].Value = items[c - 4].Asunto;
                        excelWorksheet.Cells[c, 5].Value = items[c - 4].Descripcion;
                        excelWorksheet.Cells[c, 7].Value = items[c - 4].Usuario.Telefono;
                    }

                    FileInfo newfile = new FileInfo(newfileName);
                    package.SaveAs(newfile);
                    byte[] data = package.GetAsByteArray();

                    byte[] fileBytes = System.IO.File.ReadAllBytes(newfileName);
                    return File(fileBytes, "application/x-msdownload", newfile.FullName);



                }


            }
            catch (Exception)
            {
                return null;
            }
        }

        private static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private string GetMimeType(string file)
        {
            string extension = Path.GetExtension(file).ToLowerInvariant();
            switch (extension)
            {
                case ".txt": return "text/plain";
                case ".pdf": return "application/pdf";
                case ".doc": return "application/vnd.ms-word";
                case ".docx": return "application/vnd.ms-word";
                case ".xls": return "application/vnd.ms-excel";
                case ".png": return "image/png";
                case ".jpg": return "image/jpeg";
                case ".jpeg": return "image/jpeg";
                case ".gif": return "image/gif";
                case ".csv": return "text/csv";
                default: return "";
            }
        }
    }


}


