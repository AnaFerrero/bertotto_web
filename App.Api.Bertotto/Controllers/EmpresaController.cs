﻿using ClienteBB.BD.DAL;
using ClienteBB.BD.Models;
using ClienteBB.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ClienteBB.Web.Controllers
{

    [ApiController]
    [Produces("application/json")]
    public class EmpresaController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        public EmpresaController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //[HttpGet]
        [Route("api/Empresa/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var models = await DB.Empresas.ToListAsync();
                return new OkObjectResult(models);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }


        //[HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Empresa/GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var model = await DB.Empresas.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (model == null)
                {
                    return NotFound();
                }

                return new OkObjectResult(model);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return NotFound();
            }
            finally
            {
                DB.Dispose();
            }
        }


        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Empresa/GetByCuit/{Cuit}")]
        public async Task<IActionResult> GetByCuit(string Cuit)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.Empresas.Where(x => x.CUIT == Cuit).FirstOrDefaultAsync();

                if (entity == null)
                {
                    return NotFound();
                }
                return new OkObjectResult(entity);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return NotFound();
            }
            finally
            {
                DB.Dispose();
            }
        }


        [Route("api/Empresa/GetLocalidades")]
        public async Task<IActionResult> GetLocalidades()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var models = await DB.Localidades.ToListAsync();
                return new OkObjectResult(models);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }


        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]

        [Route("api/Empresa/Create")]
        public async Task<IActionResult> Create([FromBody] Empresa entity)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var Empresa = await DB.Empresas.Where(x => x.CUIT == entity.CUIT).FirstOrDefaultAsync();

                if (Empresa != null)
                {
                    return Conflict(new { message = "El CUIT ya se encuentra registrado.<br>Verifique por favor." });
                }


                if (entity.FechaAlta == null)
                {
                    entity.FechaAlta = DateTime.Today;
                }




                DB.Add(entity);
                await DB.SaveChangesAsync();

                return CreatedAtAction(nameof(GetById),
                                   new { id = entity.Id }, entity);

            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });

            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPut]
        [Route("api/Empresa/Update")]
        public async Task<IActionResult> Update([FromBody] Empresa entity)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                DB.Entry(entity).State = EntityState.Modified;
                await DB.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpDelete]
        [Route("api/Empresa/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.Empresas.FindAsync(id);
                if (entity == null)
                {
                    return NotFound();
                }

                DB.Empresas.Remove(entity);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }
    }
}