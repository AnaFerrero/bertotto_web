﻿using ClienteBB.BD.DAL;
using ClienteBB.Web.VModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Security;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteBB.Web.Controllers
{

    [ApiController]
    [Produces("application/json")]
    public class LoginController : ControllerBase
    {



        [HttpPost("login")]
        [Route("api/Login/Login")]
        public async Task<IActionResult> Login([FromBody]LoginVModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ClienteBBContext DB = new ClienteBBContext();

            var user = await DB.Usuarios.Where(x => x.Email == model.username && x.Password == model.password).Include(x => x.Rol).FirstOrDefaultAsync();

            if (user == null)
            {
                return BadRequest(new { message = "Usuario o contraseña incorrecta." });
            }

            model.token = TokenGenerator.GenerateTokenJwt(model.username, user.Rol.Nombre);
            model.id = user.Id;
            model.rol = user.Rol.Nombre;

            var json = JsonConvert.SerializeObject(model);
            return new OkObjectResult(json);
        }


    }
}