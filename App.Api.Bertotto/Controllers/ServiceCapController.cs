﻿using ClienteBB.BD.DAL;
using ClienteBB.BD.Models;
using ClienteBB.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteBB.Web.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class ServiceCapController : ControllerBase
    {

        [HttpGet]
        [Route("api/ServiceCap/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(await DB.ServiceCaps.Select(x => new ServiceCap
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Direccion = x.Direccion,
                    Email = x.Email,
                    Idioma = x.Idioma,
                    Activo = x.Activo
                }).Include(x => x.Idioma).OrderBy(x => x.Nombre).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }


        [HttpGet]
        [Route("api/ServiceCap/GetActiveAll")]
        public async Task<IActionResult> GetActiveAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                return new OkObjectResult(await DB.ServiceCaps.Where(x => x.Activo == true).Include(x => x.Idioma)
                       .Select(s => new ServiceCap
                       {
                           Nombre = s.Nombre,
                           Id = s.Id,
                           Direccion = s.Direccion,
                           Descripcion = s.Descripcion,
                           DescripcionCorta = s.DescripcionCorta,
                           Latitud = s.Latitud,
                           Longitud = s.Longitud,
                           Provincia = s.Provincia

                       }).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpGet]
        [Route("api/ServiceCap/GetActiveByProvincia/{Provincia}")]
        public async Task<IActionResult> GetActiveByProvincia(string Provincia)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                return new OkObjectResult(await DB.ServiceCaps.Where(x => x.Activo == true && x.Provincia == Provincia).Include(x => x.Idioma)
                       .Select(s => new ServiceCap
                       {
                           Nombre = s.Nombre,
                           Id = s.Id,
                           Direccion = s.Direccion,
                           Descripcion = s.Descripcion,
                           DescripcionCorta = s.DescripcionCorta,
                           Latitud = s.Latitud,
                           Longitud = s.Longitud,
                           Provincia = s.Provincia

                       }).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        //[HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/ServiceCap/GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.ServiceCaps.Where(x => x.Id == id).FirstOrDefaultAsync();


                if (entity == null)
                {
                    return NotFound();
                }

                return new OkObjectResult(entity);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }



        [HttpPost]
        [Route("api/ServiceCap/Create")]
        public async Task<IActionResult> Create([FromBody] ServiceCap model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                // model.Fecha_Alta = DateTime.Today;
                DB.Add(model);
                await DB.SaveChangesAsync();

                return CreatedAtAction(nameof(GetById),
                    new { id = 1 }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPut]
        [Route("api/ServiceCap/Update")]
        public async Task<IActionResult> Update([FromBody] ServiceCap model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entry = DB.Entry(model);
                entry.State = EntityState.Modified;
                //entry.Property("Fecha_Alta").IsModified = false;
                // cargarLatitudLatitud(constructuirGeoURL(model.Direccion));
                await DB.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }


        [HttpDelete]
        [Route("api/ServiceCap/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.ServiceCaps.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                DB.ServiceCaps.Remove(item);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/ServiceCap/GetProvincias")]
        public IActionResult GetProvincias()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(DataHelper.GetProvincias().OrderBy(x => x.Nombre));
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

        //string constructuirGeoURL(string direccion)
        //{
        //    string geoURL = "";

        //    if (direccion != "")
        //    {
        //        direccion = direccion.Trim();
        //        direccion = direccion.Replace(" ", "+");

        //        geoURL = @"http://maps.google.com/maps/geo?
        //        q=###ADDRESS###&output=###OUTPUT###&key=###KEY###";

        //        //Sustitución de las variables
        //        geoURL = geoURL.Replace("###ADDRESS###", direccion);
        //        geoURL = geoURL.Replace("###OUTPUT###", "csv");
        //        geoURL = geoURL.Replace("###KEY###", "AIzaSyCvRPW7NvkFoPP3jISW4JA1M7jUwy0ALUU");
        //    }

        //    return geoURL;
        //}
        //void cargarLatitudLatitud(string geoURL)
        //{
        //    string csvValues = "";
        //    string Latitud = "";
        //    string Longitud = "";
        //    string Respuesta_geo = "";
        //    try
        //    {
        //        WebRequest objWebRequest = WebRequest.Create(geoURL);
        //        WebResponse objWebResponse = objWebRequest.GetResponse();
        //        Stream objWebStream = objWebResponse.GetResponseStream();

        //        using (StreamReader objStreamReader = new StreamReader(objWebStream))
        //        {
        //            csvValues = objStreamReader.ReadToEnd();
        //        }

        //        if (csvValues != null)
        //        {
        //            string[] geoValues = csvValues.Split(new char[] { ',' });
        //            if (geoValues.Length > 0)
        //            {
        //                Respuesta_geo = geoValues[0].ToString();
        //                Latitud = geoValues[2].ToString();
        //                Longitud = geoValues[3].ToString();
        //            }
        //        }

        //    }
        //    catch (Exception)
        //    {

        //    }
        //}
    }


}