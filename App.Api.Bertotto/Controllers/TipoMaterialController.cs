﻿using ClienteBB.BD.DAL;
using ClienteBB.BD.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteBB.Web.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class TipoMaterialController : ControllerBase
    {

        [HttpGet]
        [Route("api/TipoMaterial/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(await DB.TipoMateriales.Select(x => new TipoMaterial
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Descripcion = x.Descripcion,
                    Idioma = x.Idioma
                }).Include(x => x.Idioma).OrderBy(x => x.Nombre).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }


        [HttpGet]
        [Route("api/TipoMaterial/GetActiveAll")]
        public async Task<IActionResult> GetActiveAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                return new OkObjectResult(await DB.TipoMateriales.Include(x => x.Idioma)
                       .Select(s => new TipoMaterial
                       {
                           Nombre = s.Nombre,
                           Id = s.Id,
                           Descripcion = s.Descripcion,
                           Idioma = s.Idioma,
                           Orden = s.Orden
                       }).OrderBy(x => x.Orden).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }
        //[HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/TipoMaterial/GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.TipoMateriales.Where(x => x.Id == id).FirstOrDefaultAsync();


                if (entity == null)
                {
                    return NotFound();
                }

                return new OkObjectResult(entity);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }



        [HttpPost]
        [Route("api/TipoMaterial/Create")]
        public async Task<IActionResult> Create([FromBody] TipoMaterial model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                // model.Fecha_Alta = DateTime.Today;
                DB.Add(model);
                await DB.SaveChangesAsync();

                return CreatedAtAction(nameof(GetById),
                    new { id = 1 }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPut]
        [Route("api/TipoMaterial/Update")]
        public async Task<IActionResult> Update([FromBody] TipoMaterial model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entry = DB.Entry(model);
                entry.State = EntityState.Modified;
                //entry.Property("Fecha_Alta").IsModified = false;
                // cargarLatitudLatitud(constructuirGeoURL(model.Direccion));
                await DB.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }


        [HttpDelete]
        [Route("api/ServiceCap/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.TipoMateriales.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                DB.TipoMateriales.Remove(item);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

    }


}