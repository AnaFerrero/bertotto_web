﻿using ClienteBB.BD.DAL;
using ClienteBB.BD.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteBB.Web.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class EventoController : ControllerBase
    {

        [HttpGet]
        [Route("api/Evento/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(await DB.Eventos.Select(x => new Evento
                {
                    Id = x.Id,
                    Fecha = x.Fecha,
                    Nombre = x.Nombre,
                    Idioma = x.Idioma,
                    Activo = x.Activo,
                    Fecha_Evento = x.Fecha_Evento
                }).Include(x => x.Idioma).OrderByDescending(x => x.Fecha_Evento).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }


        [HttpGet]
        [Route("api/Evento/GetActiveAll")]
        public async Task<IActionResult> GetActiveAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                return new OkObjectResult(await DB.Eventos.Where(x => x.Activo == true).Include(x => x.Idioma)
                       .Select(s => new Evento
                       {
                           Nombre = s.Nombre,
                           Id = s.Id,
                           Activo = s.Activo,
                           Fecha = s.Fecha,
                           Direccion = s.Direccion

                       }).OrderByDescending(x => x.Fecha_Evento).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }
        //[HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Evento/GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.Eventos.Where(x => x.Id == id).FirstOrDefaultAsync();


                if (entity == null)
                {
                    return NotFound();
                }

                return new OkObjectResult(entity);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }



        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Route("api/Evento/Create")]
        public async Task<IActionResult> Create([FromBody] Evento model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                DB.Add(model);
                await DB.SaveChangesAsync();

                return CreatedAtAction(nameof(GetById),
                    new { id = 1 }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPut]
        [Route("api/Evento/Update")]
        public async Task<IActionResult> Update([FromBody] Evento model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entry = DB.Entry(model);
                entry.State = EntityState.Modified;
                // entry.Property("Fecha_Alta").IsModified = false;

                await DB.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }


        [HttpDelete]
        [Route("api/Evento/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.Eventos.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                DB.Eventos.Remove(item);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

    }


}