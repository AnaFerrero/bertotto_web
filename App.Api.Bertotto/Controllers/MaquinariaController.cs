﻿using ClienteBB.BD.DAL;
using ClienteBB.BD.Models;
using ClienteBB.Web.Helpers;
using ClienteBB.Web.VModels;
using log4net.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;


namespace ClienteBB.Web.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class MaquinariaController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        string PathGaleria;
        string UrlManual;
        public MaquinariaController(IConfiguration configuration)
        {
            _configuration = configuration;
            PathGaleria = _configuration["Galeria"];
            UrlManual = _configuration["UrlManual"];
        }

        [HttpGet]
        [Route("api/Maquinaria/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(await DB.Maquinarias.Include(x => x.TipoMaquinaria)
                    .Include(x => x.TipoMaterial).Select(s => new MaquinariaVModel
                    {
                        Nombre = s.Nombre,
                        DescripcionCorta = s.DescripcionCorta,
                        MetaTitle = s.MetaTitle,
                        Id = s.Id,
                        Modelo = s.Modelo,
                        TipoMaquina = s.TipoMaquinaria.Nombre,
                        TipoMaterial = s.TipoMaterial.Nombre
                    }).OrderBy(x => x.Nombre).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [HttpGet]
        [Route("api/Maquinaria/GetImagenes/{id}")]
        public async Task<IActionResult> GetImagenes(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var folderName = Path.Combine("wwwroot", "Imagenes");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);


                var result = await DB.GaleriaImagenes.Where(x => (x.MaquinariaId == id)).ToListAsync();

                List<GaleriaImagenVM> images = new List<GaleriaImagenVM>();

                foreach (var _image in result)
                {
                    GaleriaImagenVM item = new GaleriaImagenVM();
                    item.Nombre = _image.Nombre;
                    item.Id = _image.Id;
                    item.Imagen = PathGaleria + _image.Imagen;
                    item.Principal = _image.Principal;
                    images.Add(item);
                }


                return new OkObjectResult(images);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

        private string GetImagenPortada(int MaquinariaId)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var imagen = DB.GaleriaImagenes.Where(x => (x.MaquinariaId == MaquinariaId && x.Principal == true))
                       .Select(s => new GaleriaImagenVM
                       {
                           Imagen = PathGaleria + "/" + s.Imagen

                       }).SingleOrDefault();

                if (imagen == null)
                {
                    imagen = DB.GaleriaImagenes.Where(x => (x.MaquinariaId == MaquinariaId))
                           .Select(s => new GaleriaImagenVM
                           {
                               Imagen = PathGaleria + "/" + s.Imagen

                           }).SingleOrDefault();
                }


                return imagen.Imagen;
            }
            catch (Exception)
            {

                return "";
            }

        }
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Maquinaria/GetTipos")]
        public IActionResult GetTipoMaquinas()
        {

            try
            {
                return new OkObjectResult(DataHelper.GetTipoMaquinas());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {

            }

        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Maquinaria/GetTiposMateriales")]
        public IActionResult GetTiposMateriales()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(DB.TipoMateriales.ToList());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Maquinaria/GetTiposByMaterial/{MaterialId}")]
        public IActionResult GetTiposMateriales(int MaterialId)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(DB.TipoMaquinarias.Where(x=> x.TipoMaterial.Id == MaterialId).ToList());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }
        [HttpGet]
        [Route("api/Maquinaria/GetActiveAll")]
        public async Task<IActionResult> GetActiveAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                var result = await DB.Maquinarias.Where(x => (x.Activo == true)).OrderBy(x => x.TipoId).ToListAsync();

                foreach (var _item in result)
                {
                    _item.Imagen = GetImagenPortada(_item.Id);
                }


                return new OkObjectResult(result);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }
        [HttpGet]
        [Route("api/Maquinaria/GetMaquinariaRelacionada/{MaquinariaId}")]
        public async Task<IActionResult> GetMaquinariaRelacionada(int MaquinariaId)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                List<Maquinaria> result = new List<Maquinaria>();

                result = await DB.Maquinarias.Where(x =>
                       (x.Activo == true && x.Id != MaquinariaId))
                        .Include(x => x.TipoMaquinaria).Take(4).OrderBy(x => x.Nombre).ToListAsync();

                foreach (var _item in result)
                {
                    _item.Imagen = GetImagenPortada(_item.Id);
                }
                return new OkObjectResult(result);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpGet]
        [Route("api/Maquinaria/GetByTipo/{TipoMaquinaria}")]
        public async Task<IActionResult> GetByTipo(int TipoMaquinaria)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {


                var result = await DB.Maquinarias.Where(x =>
                              x.TipoId == TipoMaquinaria)
                              .Include(x => x.TipoMaquinaria)
                               .Include(x => x.TipoMaterial)
                             .Select(s => new MaquinariaVModel
                             {
                                 Nombre = s.Nombre,
                                 DescripcionCorta = s.DescripcionCorta,
                                 MetaTitle = s.MetaTitle,
                                 Id = s.Id,
                                 Modelo = s.Modelo,
                                 TipoMaquina = s.TipoMaquinaria.Nombre,
                                 TipoMaterial = (s.TipoMaterial != null ? s.TipoMaterial.Nombre : "")
                             }).OrderBy(x => x.Nombre).ToListAsync();


                return new OkObjectResult(result);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }
        [HttpGet]
        [Route("api/Maquinaria/GetActiveByTipo/{TipoMaquinaria}/{Estado}")]
        public async Task<IActionResult> GetActiveByTipo(int TipoMaquinaria, string Estado)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                List<Maquinaria> result = new List<Maquinaria>();
                if (Estado == "All")
                {

                    result = await DB.Maquinarias.Where(x =>
                            x.TipoId == TipoMaquinaria)
                            .Include(x => x.TipoMaquinaria)
                            .OrderBy(x => x.Nombre).ToListAsync();
                }
                else
                {
                    result = await DB.Maquinarias.Where(x =>
                      (x.Activo == true) && x.TipoId == TipoMaquinaria
                      && x.Estado == Estado)
                      .Include(x => x.TipoMaquinaria).OrderBy(x => x.Nombre).ToListAsync();

                }


                foreach (var _item in result)
                {
                    _item.Imagen = GetImagenPortada(_item.Id);
                }
                return new OkObjectResult(result);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpGet]
        [Route("api/Maquinaria/GetActiveByEstado/{Estado}")]
        public async Task<IActionResult> GetActiveByEstado(string Estado)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                List<Maquinaria> result = new List<Maquinaria>();
                if (Estado == "All")
                {

                    result = await DB.Maquinarias.Where(x =>
                       (x.Activo == true))
                        .Include(x => x.TipoMaquinaria).OrderBy(x => x.TipoId).ToListAsync();
                }
                else
                {
                    result = await DB.Maquinarias.Where(x =>
                      (x.Activo == true)
                      && x.Estado == Estado)
                      .Include(x => x.TipoMaquinaria).OrderBy(x => x.TipoId).ToListAsync();

                }

                foreach (var _item in result)
                {
                    _item.Imagen =  GetImagenPortada(_item.Id);
                }
                return new OkObjectResult(result);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpGet]
        [Route("api/Maquinaria/GetMaquinaria/{Meta}")]
        public async Task<IActionResult> GetMaquinaria(string Meta)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                var folderName = Path.Combine("wwwroot", "Imagenes");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                var result = await DB.Maquinarias.Where(x =>
                      (x.MetaTitle == Meta))
                      .Select(s => new MaquinariaVModel
                      {
                          Nombre = s.Nombre,
                          DescripcionCorta = s.DescripcionCorta,
                          Descripcion = s.Descripcion,
                          MetaTitle = s.MetaTitle,
                          Id = s.Id,
                          Imagen = GetImagenPortada(s.Id),
                          Activo = s.Activo,
                          UrlFolleto = PathGaleria + s.UrlFolleto,
                          UrlManual = s.UrlManual,
                          TipoMaquina = s.TipoMaquinaria.Nombre
                      }).SingleOrDefaultAsync();

                result.Imagenes = await DB.GaleriaImagenes.Where(x =>
                    (x.MaquinariaId == result.Id))
                      .Select(s => new GaleriaImagenVM
                      {
                          Nombre = s.Nombre,
                          Id = s.Id,
                          Imagen = PathGaleria + s.Imagen

                      }).ToListAsync();


                return new OkObjectResult(result);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }
        //[HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Maquinaria/GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.Maquinarias.Where(x => x.Id == id).FirstOrDefaultAsync();


                if (entity == null)
                {
                    return NotFound();
                }

                return new OkObjectResult(entity);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }



        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Route("api/Maquinaria/Create")]
        public async Task<IActionResult> Create([FromBody] Maquinaria model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                DB.Add(model);
                await DB.SaveChangesAsync();


                model.MetaTitle = Helper.GetNameUrl(model.Nombre, model.Id);

                if (model.UrlManual != "")
                {
                    model.UrlPublicaManual = UrlManual + model.MetaTitle;
                }
                await DB.SaveChangesAsync();


                return CreatedAtAction(nameof(GetById),
                    new { id = 1 }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPut]
        [Route("api/Maquinaria/Update")]
        public async Task<IActionResult> Update([FromBody] Maquinaria model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                if (string.IsNullOrEmpty(model.MetaTitle))
                {
                    model.MetaTitle = Helper.GetNameUrl(model.Nombre, model.Id);
                }

                if (model.UrlManual != "")
                {
                    model.UrlPublicaManual = UrlManual + model.MetaTitle;
                }

                var entry = DB.Entry(model);
                entry.State = EntityState.Modified;
                DB.Entry(model).Property(x => x.UrlFolleto).IsModified = false;
                DB.Entry(model).Property(x => x.UrlManual).IsModified = true;
                await DB.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }
        [HttpPost, DisableRequestSizeLimit]
        [Route("api/Maquinaria/UploadImagen/{id}")]
        public IActionResult UploadImagen(int Id)
        {
            ClienteBBContext DB = new ClienteBBContext();

            try
            {
                var file = Request.Form.Files[0];

                //System.IO.Stream fs = file.OpenReadStream();
                //System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                //Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

                var folderName = Path.Combine("wwwroot", "Imagenes");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);


                if (file.Length > 0)
                {
                    try
                    {
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }

                        string nombre = DB.Maquinarias.Where(x => x.Id == Id).SingleOrDefault().MetaTitle + "-" + DB.GaleriaImagenes.Where(x => x.MaquinariaId == Id).Count().ToString();



                        GaleriaImagen image = new GaleriaImagen();
                        image.Imagen = fileName;
                        image.MaquinariaId = Id;
                        image.Nombre = nombre;
                        DB.GaleriaImagenes.Add(image);
                        DB.SaveChanges();


                    }
                    catch (Exception ex)
                    {
                        return BadRequest(new { message = ex.InnerException });
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }

        }

      
        [HttpPost, DisableRequestSizeLimit]
        [Route("api/Maquinaria/UpdateDocumento/{id}/{tipoDocumento}")]
        public async Task<IActionResult> UpdateDocumento(int Id, string tipoDocumento)
        {
            ClienteBBContext DB = new ClienteBBContext();

            try
            {
                var file = Request.Form.Files[0];


                var folderName = Path.Combine("wwwroot", "Imagenes");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);


                if (file.Length > 0)
                {
                    try
                    {
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }


                        var entity = await DB.Maquinarias.Where(x => x.Id == Id).FirstOrDefaultAsync();


                        if (entity != null)
                        {

                            if (tipoDocumento == "Folleto")
                            {
                                entity.UrlFolleto = fileName;
                            }
                            if (tipoDocumento == "Manual")
                            {
                                entity.UrlManual = fileName;
                            }
                            await DB.SaveChangesAsync();
                        }


                    }
                    catch (Exception ex)
                    {
                        return BadRequest(new { message = ex.InnerException });
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }

        }

        [HttpGet]
        [Route("api/Maquinaria/DownloadPdf/{Id}/{tipoDocumento}")]
        public async Task<IActionResult> DownloadPdf(int Id, string tipoDocumento)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {


                var folderName = Path.Combine("wwwroot", "Imagenes");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                var entity = await DB.Maquinarias.Where(x => x.Id == Id).FirstOrDefaultAsync();

                string FileToReturn = "";

                if (entity != null)
                {

                    if (tipoDocumento == "Folleto")
                    {
                        FileToReturn = pathToSave + @"\" + entity.UrlFolleto;
                    }
                    if (tipoDocumento == "Manual")
                    {
                        FileToReturn = pathToSave + @"\" + entity.UrlManual;
                    }

                }

                var bytes = System.IO.File.ReadAllBytes(FileToReturn);
                //return new FileStream(FileToReturn, FileMode.Open, FileAccess.Read);

                System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = FileToReturn

                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                Response.Headers.Add("X-Content-Type-Options", "nosniff");

                return File(bytes, "application/pdf");
            }
            catch (Exception)
            {

                throw;
            }


        }
        [HttpDelete]
        [Route("api/Maquinaria/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.Maquinarias.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                DB.Maquinarias.Remove(item);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [HttpDelete]
        [Route("api/Maquinaria/UpdateImagen/{id}")]
        public async Task<IActionResult> UpdateImagen(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.GaleriaImagenes.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                var galeria = await DB.GaleriaImagenes.Where(x => x.MaquinariaId == item.MaquinariaId).ToListAsync();

                foreach (var _galeria in galeria)
                {
                    _galeria.Principal = false;
                }

                item.Principal = true;
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [HttpDelete]
        [Route("api/Maquinaria/DeleteImagen/{id}")]
        public async Task<IActionResult> DeleteImagen(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.GaleriaImagenes.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                DB.GaleriaImagenes.Remove(item);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }
    }
}
