﻿using ClienteBB.BD.DAL;
using ClienteBB.BD.Models;
using ClienteBB.Web.Helpers;
using ClienteBB.Web.VModels;
using log4net.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ClienteBB.Web.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class NovedadController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        string PathGaleria;
        public NovedadController(IConfiguration configuration)
        {
            _configuration = configuration;
            //_logger = ilogger;
            PathGaleria = _configuration["Galeria"];
        }

        [HttpGet]
        [Route("api/Novedad/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(await DB.Novedades.Select(x => new Novedad
                {
                    Id = x.Id,
                    Fecha_Alta = x.Fecha_Alta,
                    Fecha_Hasta_Notificacion = x.Fecha_Hasta_Notificacion,
                    Nombre = x.Nombre,
                    Idioma = x.Idioma,
                    Activo = x.Activo
                }).Include(x => x.Idioma).OrderByDescending(x => x.Fecha_Alta).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [HttpGet]
        [Route("api/Novedad/GetImagenes/{id}")]
        public async Task<IActionResult> GetImagenes(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var folderName = Path.Combine("wwwroot", "Imagenes");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);


                var result = await DB.GaleriaImagenes.Where(x => (x.NoticiaId == id)).ToListAsync();

                List<GaleriaImagenVM> images = new List<GaleriaImagenVM>();

                foreach (var _image in result)
                {
                    GaleriaImagenVM item = new GaleriaImagenVM();
                    item.Nombre = _image.Nombre;
                    item.Id = _image.Id;
                    item.Principal = _image.Principal;
                    item.Imagen = PathGaleria + _image.Imagen;
                    images.Add(item);
                }


                return new OkObjectResult(images);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [HttpDelete]
        [Route("api/Novedad/UpdateImagen/{id}")]
        public async Task<IActionResult> UpdateImagen(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.GaleriaImagenes.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                var galeria = await DB.GaleriaImagenes.Where(x => x.NoticiaId == item.NoticiaId).ToListAsync();

                foreach (var _galeria in galeria)
                {
                    _galeria.Principal = false;
                }

                item.Principal = true;
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }

        }
        [HttpGet]
        [Route("api/Novedad/GetActiveAll")]
        public async Task<IActionResult> GetActiveAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                return new OkObjectResult(await DB.Novedades.Where(x =>
                      (x.Activo == true)).Include(x => x.Idioma)
                       .Select(s => new NovedadVModel
                       {
                           Nombre = s.Nombre,
                           DescripcionCorta = s.DescripcionCorta,
                           MetaTitle = s.MetaTitle,
                           Id = s.Id,
                           Imagen = GetImagenPortada(s.Id),
                           Activo = s.Activo,
                           Fecha_Hasta_Notificacion = s.Fecha_Hasta_Notificacion
                       }).OrderByDescending(x => x.Fecha_Alta).ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }
        }

        private string GetImagenPortada(int NovedadId)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var imagen = DB.GaleriaImagenes.Where(x => (x.NoticiaId == NovedadId && x.Principal == true))
                       .Select(s => new GaleriaImagenVM
                       {
                           Imagen = PathGaleria + "/" + s.Imagen

                       }).SingleOrDefault();
                if (imagen == null)
                {
                    imagen = DB.GaleriaImagenes.Where(x => (x.NoticiaId == NovedadId))
                           .Select(s => new GaleriaImagenVM
                           {
                               Imagen = PathGaleria + "/" + s.Imagen

                           }).SingleOrDefault();
                }
                return imagen.Imagen;
            }
            catch (Exception ex)
            {
                return ex.InnerException.ToString();
            }
        }



        [HttpGet]
        [Route("api/Novedad/GetNovedad/{Meta}")]
        public async Task<IActionResult> GetNovedad(string Meta)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {

                var folderName = Path.Combine("wwwroot", "Imagenes");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                var result = await DB.Novedades.Where(x =>
                      (x.MetaTitle == Meta))
                      .Select(s => new NovedadVModel
                      {
                          Nombre = s.Nombre,
                          DescripcionCorta = s.DescripcionCorta,
                          Descripcion = s.Descripcion,
                          MetaTitle = s.MetaTitle,
                          Id = s.Id,
                          Imagen = GetImagenPortada(s.Id),
                          Activo = s.Activo,
                          Fecha_Hasta_Notificacion = s.Fecha_Hasta_Notificacion
                      }).SingleOrDefaultAsync();

                result.Imagenes = await DB.GaleriaImagenes.Where(x =>
                    (x.NoticiaId == result.Id))
                      .Select(s => new GaleriaImagenVM
                      {
                          Nombre = s.Nombre,
                          Id = s.Id,
                          Imagen = PathGaleria + "/" + s.Imagen

                      }).ToListAsync();


                return new OkObjectResult(result);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }
        }
        //[HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Novedad/GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.Novedades.Where(x => x.Id == id).FirstOrDefaultAsync();


                if (entity == null)
                {
                    return NotFound();
                }

                return new OkObjectResult(entity);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Novedad/GetIdiomas")]
        public async Task<IActionResult> GetIdiomas()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(await DB.Idiomas.ToListAsync());
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }

        }




        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Route("api/Novedad/Create")]
        public async Task<IActionResult> Create([FromBody] Novedad model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                model.MetaTitle = Helper.GetNameUrl(model.Nombre, model.Id);
                model.Fecha_Alta = DateTime.Today;
                DB.Add(model);
                await DB.SaveChangesAsync();

                return CreatedAtAction(nameof(GetById),
                    new { id = 1 }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPut]
        [Route("api/Novedad/Update")]
        public async Task<IActionResult> Update([FromBody] Novedad model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                if (string.IsNullOrEmpty(model.MetaTitle))
                {
                    model.MetaTitle = Helper.GetNameUrl(model.Nombre, model.Id);
                }

                var entry = DB.Entry(model);
                entry.State = EntityState.Modified;
                entry.Property("Fecha_Alta").IsModified = false;

                await DB.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }
        }
        [HttpPost, DisableRequestSizeLimit]
        [Route("api/Novedad/UploadImagen/{id}")]
        public IActionResult UploadImagen(int Id)
        {
            ClienteBBContext DB = new ClienteBBContext();

            try
            {
                var file = Request.Form.Files[0];

                //System.IO.Stream fs = file.OpenReadStream();
                //System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                //Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

                var folderName = Path.Combine("wwwroot", "Imagenes");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);


                if (file.Length > 0)
                {
                    try
                    {
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(new { message = ex.InnerException });
                    }
                }

                string nombre = DB.Novedades.Where(x => x.Id == Id).SingleOrDefault().MetaTitle + "-" + DB.GaleriaImagenes.Where(x => x.Id == Id).Count().ToString();



                GaleriaImagen image = new GaleriaImagen();
                image.Imagen = fileName;
                image.NoticiaId = Id;
                image.Nombre = nombre;
                DB.GaleriaImagenes.Add(image);
                DB.SaveChanges();

                return Ok();
            }
            catch (Exception ex)
            {

                return BadRequest(new { message = ex.InnerException });
            }

        }

        [HttpDelete]
        [Route("api/Novedad/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.Novedades.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                DB.Novedades.Remove(item);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [HttpDelete]
        [Route("api/Novedad/DeleteImagen/{id}")]
        public async Task<IActionResult> DeleteImagen(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.GaleriaImagenes.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                DB.GaleriaImagenes.Remove(item);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.InnerException });
            }
            finally
            {
                DB.Dispose();
            }

        }
    }
}
