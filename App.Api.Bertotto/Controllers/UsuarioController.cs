﻿using ClienteBB.BD.DAL;
using ClienteBB.BD.Models;
using ClienteBB.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ClienteBB.Web.Controllers
{

    [ApiController]
    [Produces("application/json")]
    public class UsuarioController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        public UsuarioController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //[HttpGet]
        [Route("api/Usuario/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var models = await DB.Usuarios.Include(x => x.Rol).ToListAsync();
                return new OkObjectResult(models);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }


        //[HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Usuario/GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var model = await DB.Usuarios.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (model == null)
                {
                    return NotFound();
                }
                model.ConfirmPassword = model.Password;
                return new OkObjectResult(model);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return NotFound();
            }
            finally
            {
                DB.Dispose();
            }
        }




        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Usuario/RecoverPassword")]
        public async Task<IActionResult> RecoverPassword(string Email)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.Usuarios.Where(x => x.Email == Email).FirstOrDefaultAsync();

                if (entity != null)
                {
                    var KEY = _configuration.GetSection("API_Key").Value;
                    var client = new SendGridClient(KEY);
                    Email = _configuration.GetSection("Email").Value;

                    var from = new EmailAddress("noreply@ClienteBB.com.ar", "ClienteBB");
                    var to = new EmailAddress(entity.Email, entity.Nombre);


                    var subject = "Recuperar de contraseña";
                    var htmlContent = "Su contraseña actual es:" + entity.Password;

                    var msg = MailHelper.CreateSingleEmail(from, to, subject, "", htmlContent);
                    var response = await client.SendEmailAsync(msg);


                    if (response.StatusCode != HttpStatusCode.Accepted)
                    {
                        return BadRequest(new { message = response.StatusCode });
                    }

                    return Ok(new { message = "Mensaje enviado con éxito." });
                }
                else
                {
                    return BadRequest(new { message = "Usuario no existe." });
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }

        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Usuario/IsExistUser/{Cuit}")]
        public async Task<IActionResult> IsExistUser(string Email)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.Usuarios.Where(x => x.Email == Email).FirstOrDefaultAsync();

                if (entity == null)
                {
                    return NotFound(new { message = "false" });
                }

                return Ok(new { message = "true" });
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return NotFound();
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]

        [Route("api/Usuario/Create")]
        public async Task<IActionResult> Create([FromBody] Usuario entity)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var usuario = await DB.Usuarios.Where(x => x.Email == entity.Email).FirstOrDefaultAsync();

                if (usuario != null)
                {
                    return Conflict(new { message = "El CUIT ya se encuentra registrado.<br>Verifique por favor." });
                }


                if (entity.FechaAlta == null)
                {
                    entity.FechaAlta = DateTime.Today;
                }

                if (entity.RolId == 0)
                {
                    entity.RolId = DB.Roles.Where(x => x.Nombre == "Viewer").SingleOrDefault().Id;
                }


                DB.Add(entity);
                await DB.SaveChangesAsync();

                return CreatedAtAction(nameof(GetById),
                                   new { id = entity.Id }, entity);

            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });

            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPut]
        [Route("api/Usuario/Update")]
        public async Task<IActionResult> Update([FromBody] Usuario entity)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                DB.Entry(entity).State = EntityState.Modified;
                await DB.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpDelete]
        [Route("api/Usuario/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var entity = await DB.Usuarios.FindAsync(id);
                if (entity == null)
                {
                    return NotFound();
                }

                DB.Usuarios.Remove(entity);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }
    }
}