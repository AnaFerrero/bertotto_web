﻿using ClienteBB.BD.DAL;
using ClienteBB.BD.Models;
using ClienteBB.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteBB.Web.Controllers
{

    [ApiController]
    [Produces("application/json")]
    public class RolController : ControllerBase
    {


        [HttpGet]
        [Route("api/Rol/GetAll")]
        public async Task<IActionResult> GetAll()
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                return new OkObjectResult(await DB.Roles.ToListAsync());
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return NotFound();
            }
            finally
            {
                DB.Dispose();
            }

        }

        //[HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Route("api/Rol/GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var model = await DB.Roles.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (model == null)
                {
                    return NotFound();
                }
                return new OkObjectResult(model);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [Route("api/Rol/Create")]
        public async Task<IActionResult> Create([FromBody] Rol model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                DB.Add(model);
                await DB.SaveChangesAsync();

                return CreatedAtAction(nameof(GetById),
                    new { id = model.Id }, model);
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpPut]
        [Route("api/Rol/Update")]
        public async Task<IActionResult> Update([FromBody] Rol model)
        {

            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                DB.Entry(model).State = EntityState.Modified;
                await DB.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }
        }

        [HttpDelete]
        [Route("api/Rol/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ClienteBBContext DB = new ClienteBBContext();
            try
            {
                var item = await DB.Roles.FindAsync(id);
                if (item == null)
                {
                    return NotFound();
                }

                DB.Roles.Remove(item);
                await DB.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.StackTrace);
                return BadRequest(new { message = ex.Message });
            }
            finally
            {
                DB.Dispose();
            }

        }
    }
}