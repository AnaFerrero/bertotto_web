export class Evento {

  id: number;
  nombre: string;
  direccion: string;
  fecha: string;
  fecha_Evento: Date;
  activo: boolean;
  idiomaId: number;
}

