export class ServiceCap {
  id: number;
  nombre: string;
  direccion:string;
  latitud: string;
  longitud: string;
  email: string;
  descripcion: string;
  activo: boolean;
  idiomaId: number;
  fecha_Alta: Date;
  descripcionCorta: string;
  provincia: string;
}



