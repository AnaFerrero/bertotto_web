import { Usuario } from "./usuario.model";

export class Contacto {

  id: number;
  asunto: string;
  descripcion: string;
  usuario: Usuario;
  fecha: Date

  get firmaUsuario() {
    return this.usuario.nombre;
  }
 get contactoUsuario()  {
    return this.usuario.telefono + " - " + this.usuario.email;
  }
}
