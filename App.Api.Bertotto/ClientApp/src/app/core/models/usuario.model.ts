import { Rol } from "./rol.model";

export class Usuario  {

  id: number;
  nombre: string;
  password: string;
  confirmPassword?: string;
  email: string;
  telefono: string;
  terminosCondiciones: number;
  rolId: number;
  rol: Rol;
}



