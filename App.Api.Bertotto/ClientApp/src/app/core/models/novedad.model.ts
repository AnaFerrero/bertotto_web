export class Novedad {

  id: number;
  nombre: string;
  descripcion: string;
  descripcionCorta: string;
  fecha_Alta: Date;
  imagen: string;
  fecha_Desde_Notificacion: Date;
  fecha_Hasta_Notificacion: Date;
  activo: boolean;
  idiomaId: number
}

