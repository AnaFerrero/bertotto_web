export class UserSession {
  public token: string;
  public username: string;
  public password: string;
  public rol: string;
}
