import { Usuario } from "./usuario.model";


export class Turno {
  public id: number;
  public fecha: Date;
  public hora_Desde: string;
  public hora_Hasta: string;
  public usuarioId: number;
  public usuario: Usuario;
  public estadoId: number;
  public estadoTurno: EstadoTurno;
  public descripcion: string;
  public imagen: string;
  public tipoMaquinaId: number;
  public nro_Chasis: string;
}

export class EstadoTurno {
  id: number;
  nombre: string;
}

export class FilterTurno {
  public fecha_Desde: string;
  public fecha_Hasta: string;
  public estadoId: number;
}
