export class Maquinaria {

  id: number;
  nombre: string;
  descripcion: string;
  descripcionCorta: string;
  fecha_Alta: Date;
  imagen: string;
  activo: boolean;
  idiomaId: number;
  tipoId: number;
  materialId: number;
  urlFolleto: string;
  urlManual: string;
  modelo:string;
  estado:string;
  urlPublicaManual:string;
}

