
export class Empresa  {

  id: number;
  nombre: string;
  cuit: string;
  firma: string;
  password: string;
  confirmPassword?: string;
  email: string;
  telefono: string;
  localidadId: number;
  direccion: string;
  numero: string;
}


