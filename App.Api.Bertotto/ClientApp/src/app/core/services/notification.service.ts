import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

 /*  constructor(public snackBar: MatSnackBar) { }

  config: MatSnackBarConfig = {
    duration: 5000,
    horizontalPosition: 'right',
    verticalPosition: 'top'
 }


  success(msg) {
    this.config['panelClass'] = ['notification', 'success'];
    this.snackBar.open(msg, '', this.config);
  }

  warn(msg) {
    this.config['panelClass'] = ['notification', 'warn'];
    this.snackBar.open(msg, '', this.config);
  }

  error(msg) {
    this.config['panelClass'] = ['notification', 'error'];
    this.snackBar.open(msg, '', this.config);
  } */
 constructor(private toastr: ToastrService) { }

  showSuccess(message, title){
    this.toastr.success(message, title, {
      timeOut: 10000,
      positionClass: 'toast-top-right'
    });
   }

   showSuccessWithTimeout(message, title, timespan){
   this.toastr.success(message, title ,{
      timeOut : timespan
    })
   }

   showHTMLMessage(message, title){
     this.toastr.success(message, title, {
       enableHtml : true
   })
   }

   error(message) {
    this.toastr.error(message, 'Error', {
      enableHtml : true
  })
  } 
}
