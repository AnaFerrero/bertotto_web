import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



import { BaseService } from './base.service';
import { Evento } from '../models/evento.model';


@Injectable({
  providedIn: 'root'
})

export class EventoService extends BaseService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  add(body: Evento) {

    return this.http.post<Evento>(this.baseUrl + 'api/Evento/Create', body)
      .pipe(
        map(res => {
          console.log(res);
          return res;
        }));

  }

  update(body: Evento) {
    return this.http.put<Evento>(this.baseUrl + 'api/Evento/Update', body)
      .pipe(
        map(res => {
          console.log("");
          return res;
        }));
  }

  delete(id: Number) {
    return this.http.delete(this.baseUrl + "api/Evento/Delete/" + id)
      .pipe(
        map(res => {
          console.log("");
          return res;
        }));
  }


  getById(id: number) {
    return this.http.get<any>(this.baseUrl + 'api/Evento/GetById/' + id);
  }

  getAll() {
    return this.http.get<any>(this.baseUrl + 'api/Evento/GetAll');
  }

 
  getIdiomas() {
    return this.http.get<any>(this.baseUrl + 'api/Novedad/GetIdiomas');
  }
}
