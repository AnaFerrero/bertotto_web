import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



import { BaseService } from './base.service';
import { Turno, EstadoTurno, FilterTurno } from '../models/turno.model';


@Injectable({
  providedIn: 'root'
})
export class TurnoService extends BaseService {
  [x: string]: any;



  constructor(protected http: HttpClient) {
    super(http);
  }

  add(body: Turno) {

    return this.http.post<Turno>(this.baseUrl + 'api/Turno/Create', body)
      .pipe(
        map(res => {
          console.log(res);
          return res;
        }));

  }

  update(body: Turno) {
    return this.http.put<Turno>(this.baseUrl + 'api/Turno/Update', body)
      .pipe(
        map(res => {
          console.log("");
          return res;
        }));
  }

  delete(id: Number) {
    return this.http.delete(this.baseUrl + "api/Turno/Delete/" + id)
      .pipe(
        map(res => {
          console.log("");
          return res;
        }));
  }


  getById(id: number) {
    return this.http.get<Turno>(this.baseUrl + 'api/Turno/GetById/' + id);
  }

  getAll() {
    return this.http.get<Turno[]>(this.baseUrl + 'api/Turno/GetAll');
  }

  getFilterAll(fecha_Desde:string, fecha_Hasta:string, estadoId:number) {
    return this.http.get<Turno[]>(this.baseUrl + 'api/Turno/GetFilterAll?fecha_Desde=' + fecha_Desde + '&fecha_Hasta=' + fecha_Hasta + '&estadoId=' + estadoId);
  }

  //getFilterAll(body: FilterTurno) {
  //  return this.http.get<Turno[]>(this.baseUrl + 'api/Turno/GetFilterAll', body);
  //}
 
  getEstados() {
    return this.http.get<[EstadoTurno]>(this.baseUrl + 'api/Turno/GetEstadoAll');
  }
}
