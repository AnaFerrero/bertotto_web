import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';import { BaseService } from './base.service';
import { tipo } from '../models/tipo.model';


@Injectable({
  providedIn: 'root'
})

export class TipoMaquinariaService extends BaseService {

  constructor(protected http: HttpClient) {
    super(http);
  }

    add(body: tipo) {

      return this.http.post<tipo>(this.baseUrl + 'api/TipoMaquinaria/Create', body)
      .pipe(
        map(res => {
          console.log(res);
          return res;
        }));

  }

    update(body: tipo) {
        return this.http.put<tipo>(this.baseUrl + 'api/TipoMaquinaria/Update', body)
      .pipe(
        map(res => {
          console.log("");
          return res;
        }));
  }

  delete(id: Number) {
      return this.http.delete(this.baseUrl + "api/TipoMaquinaria/Delete/" + id)
      .pipe(
        map(res => {
          console.log("");
          return res;
        }));
  }


  getById(id: number) {
      return this.http.get<any>(this.baseUrl + 'api/TipoMaquinaria/GetById/' + id);
  }

  getAll() {
      return this.http.get<any>(this.baseUrl + 'api/TipoMaquinaria/GetAll');
  }
  
  getMateriales() {
    return this.http.get<any>(this.baseUrl + 'api/Maquinaria/GetTiposMateriales');
}
 
  getIdiomas() {
    return this.http.get<any>(this.baseUrl + 'api/Novedad/GetIdiomas');
  }
}
