﻿/// <reference path="../../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { FrontEndLayoutComponent } from './front-end-layout.component';

let component: FrontEndLayoutComponent;
let fixture: ComponentFixture<FrontEndLayoutComponent>;

describe('frontEnd.layout component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ FrontEndLayoutComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(FrontEndLayoutComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});