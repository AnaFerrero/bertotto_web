
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { UsuarioComponent } from './usuario.component';

let component: UsuarioComponent;
let fixture: ComponentFixture<UsuarioComponent>;

describe('usuario component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UsuarioComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(UsuarioComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});
