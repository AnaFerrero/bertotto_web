
import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TipoMaquinariaService } from '../../core/services/tipomaquinaria.service';
import { tipo } from 'src/app/core/models/tipo.model';


@Component({
    selector: 'app-tipomaquinaria-list',
    templateUrl: './tipomaquinaria-list.component.html',
    styleUrls: ['./tipomaquinaria-list.component.scss']
})
/** tipomaquinaria-list component*/
export class TipomaquinariaListComponent {

    public rows: any[];
    public temp: any[];
    public loading: boolean;

    constructor(http: HttpClient, public apiService: TipoMaquinariaService) {

        
     }


    @ViewChild(DatatableComponent) table: DatatableComponent;


    ngOnInit() {
        this.getAll();
    }

    getAll() {
        this.loading = true;

        this.apiService.getAll().subscribe(
            data => {
                this.rows = data;
                this.temp = [...data];
                this.loading = false;
            },
            () => this.loading = false);
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        const temp = this.temp.filter(function (d) {
            return d.nombre.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
        this.table.offset = 0;
    }

    delete(id: number) {
        if (confirm('Esta seguro de eliminar el registro?')) {
            this.apiService.delete(id).subscribe(() => {
                this.getAll();
            });
        }
    }
}

