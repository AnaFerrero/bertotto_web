﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { TipomaquinariaListComponent } from './tipomaquinaria-list.component';

let component: TipomaquinariaListComponent;
let fixture: ComponentFixture<TipomaquinariaListComponent>;

describe('tipomaquinaria-list component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ TipomaquinariaListComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(TipomaquinariaListComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});