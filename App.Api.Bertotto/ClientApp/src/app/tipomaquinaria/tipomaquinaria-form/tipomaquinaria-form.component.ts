import { Component } from '@angular/core';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from "@angular/common";
import { Evento } from 'src/app/core/models/evento.model';
import { EventoService } from 'src/app/core/services/evento.service';
import { Formatter } from 'src/app/core/helpers/formatter';
import { TipoMaquinariaService } from '../../core/services/tipomaquinaria.service';
import { tipo } from 'src/app/core/models/tipo.model';


@Component({
    selector: 'app-tipomaquinaria-form',
    templateUrl: './tipomaquinaria-form.component.html',
    styleUrls: ['./tipomaquinaria-form.component.scss']
})
/** tipomaquinaria-form component*/
export class TipomaquinariaFormComponent {

    /** novedad ctor */
    formGroup: FormGroup;
    title: string = "Nueva";
    id: number;
    errorMessage: any;
    submitted = false;
    idiomas: any[];
    model: tipo;
    url: any;
    formatter: Formatter;
    ckeConfig: any;
    mycontent: string;
    log: string = '';
    tipoMateriales: any[];

    public loading: boolean;

    constructor(private fb: FormBuilder,
        private apiService: TipoMaquinariaService,
        private router: Router,
        private avRoute: ActivatedRoute,
        private calendar: NgbCalendar,
        public datePipe: DatePipe) {

        this.formatter = new Formatter(this.datePipe);

        if (this.avRoute.snapshot.params["id"]) {
            this.id = this.avRoute.snapshot.params["id"];
        }
        this.createForm();

    }

    createForm() {

        this.formGroup = this.fb.group({
            'id': [0],
            'nombre': ['', Validators.required],
            'descripcion': [''],
            'idiomaId': [1],
            'materialId': [0]
        });
        
        this.getIdiomas();
    }

    get f() { return this.formGroup.controls; }


    ngOnInit() {

        this.ckeConfig = {
            allowedContent: false,
            extraPlugins: 'divarea',
            forcePasteAsPlainText: true
        };

        if (this.id > 0) {
            this.loading = true;
            this.title = "Editar";

            this.apiService.getById(this.id).subscribe(
                (resp: any) => {
                    let model = (resp as tipo)
                    console.log(model);
                    this.setForm(model);
                },
                error => {
                    this.errorMessage = error
                    console.log(error);
                },
                () => this.loading = false)
        }
    }

    setForm(form: tipo) {
        this.formGroup.get('id').setValue(form.id);
        this.formGroup.get('nombre').setValue(form.nombre);
        this.formGroup.get('idiomaId').setValue(form.idiomaId);
        this.formGroup.get('materialId').setValue(form.materialId);
        this.formGroup.get('descripcion').setValue(form.descripcion);
     }

    onSubmit() {
        this.submitted = true;

        if (this.formGroup.invalid) {
            return;
        }

        let formValues = this.formGroup.value;
       


        if (this.title == "Nueva") {
            this.loading = true
            this.apiService.add(this.formGroup.value).subscribe(
                () => {
                    this.returnUrl();
                },
                error => this.errorMessage = error,
                () => this.loading = false
            )
        }
        else if (this.title == "Editar") {
            this.loading = true
            this.apiService.update(this.formGroup.value).subscribe(
                () => {
                    this.returnUrl();
                },
                error => this.errorMessage = error,
                () => this.loading = false
            )
        }
    }

    returnUrl() {
        this.router.navigate(['/tipomaquinarias']);
    }

    cancel() {
        this.returnUrl();
    }


    getIdiomas() {
        this.apiService.getIdiomas().subscribe(
            data => {
                this.idiomas = data;
            }
        )

        this.apiService.getMateriales().subscribe(
            data => {
                this.tipoMateriales = data;
            }
        )
    }
}
