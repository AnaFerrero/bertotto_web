﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { TipomaquinariaFormComponent } from './tipomaquinaria-form.component';

let component: TipomaquinariaFormComponent;
let fixture: ComponentFixture<TipomaquinariaFormComponent>;

describe('tipomaquinaria-form component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ TipomaquinariaFormComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(TipomaquinariaFormComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});