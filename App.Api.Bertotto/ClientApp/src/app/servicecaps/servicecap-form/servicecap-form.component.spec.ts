﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { ServicecapFormComponent } from './servicecap-form.component';

let component: ServicecapFormComponent;
let fixture: ComponentFixture<ServicecapFormComponent>;

describe('servicecap-form component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ServicecapFormComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(ServicecapFormComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});