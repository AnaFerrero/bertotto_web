
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { ContactosComponent } from './contactos.component';

let component: ContactosComponent;
let fixture: ComponentFixture<ContactosComponent>;

describe('contactos component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ContactosComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(ContactosComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});
