﻿import { Component } from '@angular/core';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from "@angular/common";
import { Evento } from 'src/app/core/models/evento.model';
import { EventoService } from 'src/app/core/services/evento.service';
import { Formatter } from 'src/app/core/helpers/formatter';


@Component({
    selector: 'app-evento-form',
    templateUrl: './evento-form.component.html',
    styleUrls: ['./evento-form.component.scss']
})
/** evento-form component*/
export class EventoFormComponent {
  
    /** novedad ctor */
    formGroup: FormGroup;
    title: string = "Nueva";
    id: number;
    errorMessage: any;
    submitted = false;
    idiomas: any[];
    model: Evento;
    url: any;
    formatter: Formatter;
    ckeConfig: any;
    mycontent: string;
    log: string = '';
    
  
    public loading: boolean;
  
    constructor(private fb: FormBuilder,
      private apiService: EventoService,
      private router: Router,
      private avRoute: ActivatedRoute,
      private calendar: NgbCalendar,
      public datePipe: DatePipe) {
  
      this.formatter = new Formatter(this.datePipe);
  
      if (this.avRoute.snapshot.params["id"]) {
        this.id = this.avRoute.snapshot.params["id"];
      }
      this.createForm();
  
    }
  
    createForm() {
  
      this.formGroup = this.fb.group({
        'id': [0],
        'nombre': ['', Validators.required],
        'direccion': ['', Validators.required],
        'fecha': ['', Validators.required],
        'fecha_Evento': [this.calendar.getToday()],
        'activo': [0],
        'idiomaId':[0]
      });
    this.getIdiomas();
    }
  
    get f() { return this.formGroup.controls; }
  
  
    ngOnInit() {
  
      this.ckeConfig = {
        allowedContent: false,
        extraPlugins: 'divarea',
        forcePasteAsPlainText: true
      };
  
      if (this.id > 0) {
        this.loading = true;
        this.title = "Editar";
  
        this.apiService.getById(this.id).subscribe(
          (resp: any) => {
            let model = (resp as Evento)
            console.log(model);
            this.setForm(model);
          },
          error => {
            this.errorMessage = error
            console.log(error);
          },
          () => this.loading = false)
      }
    }
  
    setForm(form: Evento) {
      this.formGroup.get('id').setValue(form.id);
      this.formGroup.get('nombre').setValue(form.nombre);
      this.formGroup.get('idiomaId').setValue(form.idiomaId);
      this.formGroup.get('direccion').setValue(form.direccion);
      this.formGroup.get('fecha').setValue(form.fecha);
      this.formGroup.get('activo').setValue(form.activo);
      this.formGroup.get('fecha_Evento').setValue(this.formatter.FormatDate(form.fecha_Evento));
   
      
    }
  
    onSubmit() {
      this.submitted = true;
  
      if (this.formGroup.invalid) {
        return;
      }
  
      let formValues = this.formGroup.value;
     
      if (this.title == "Nueva") {
        this.loading = true
        this.apiService.add(this.formGroup.value).subscribe(
          () => {
            this.router.navigate(['/eventos']);
          },
          error => this.errorMessage = error,
          () => this.loading = false
        )
      }
      else if (this.title == "Editar") {
        this.loading = true
        this.apiService.update(this.formGroup.value).subscribe(
          () => {
            this.router.navigate(['/eventos']);
          },
          error => this.errorMessage = error,
          () => this.loading = false
        )
      }
    }
  
  
    cancel() {
      this.router.navigate(['/eventos']);
    }
  
  
    getIdiomas() {
      this.apiService.getIdiomas().subscribe(
        data => {
          this.idiomas = data;
        }
      )
    }
  
 
  }
  