import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';

//Modules///
import { AppRoutingModule } from '../app/app-routing.module';
import { AdminModule } from './core/modules/admin.module';
import { NovedadModule } from './core/modules/novedades.module';


//components///
import { AppComponent } from './app.component';
import { AuthorizatedGuard } from './Auth/auth.guard';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './shared/header/header.component';
import { HomeLayoutComponent } from './shared/layout/home.layout/home-layout.component';
import { LoginLayoutComponent } from './shared/layout/login.layout/login-layout.component';
import { FooterComponent } from './shared/footer/footer.component';
import { StorageService } from './core/services/storage.service';
import { ExcelService } from './core/helpers/ExcelService';
import { EmpresaModule } from './core/modules/empresa.module';
import { MaquinaModule } from './core/modules/maquinas.module';
import { HomeComponent } from './home/home/home.component';
import { FrontEndLayoutComponent } from './shared/layout/front-end-layout/front-end-layout.component';
import { ComponentModule } from './shared/components/component.module';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    FrontEndLayoutComponent
  ],
  exports:
    [
      
    ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserModule,
    AdminModule,
    NovedadModule,
    EmpresaModule,
    MaquinaModule,
    ComponentModule
  ],

  providers: [
    DatePipe,
    StorageService,
    ExcelService,
    AuthorizatedGuard
    
  ],
  bootstrap: [AppComponent]
})


export class AppModule { }

