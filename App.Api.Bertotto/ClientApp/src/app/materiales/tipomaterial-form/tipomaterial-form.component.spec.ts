﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { TipomaterialFormComponent } from './tipomaterial-form.component';

let component: TipomaterialFormComponent;
let fixture: ComponentFixture<TipomaterialFormComponent>;

describe('tipomaterial-form component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ TipomaterialFormComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(TipomaterialFormComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});