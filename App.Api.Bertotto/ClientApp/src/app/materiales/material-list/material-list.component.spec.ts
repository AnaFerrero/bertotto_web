﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { MaterialListComponent } from './material-list.component';

let component: MaterialListComponent;
let fixture: ComponentFixture<MaterialListComponent>;

describe('material-list component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ MaterialListComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(MaterialListComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});