import { Component, ViewChild, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Maquinaria } from '../../core/models/maquinaria.model';
import { MaquinariaService } from '../../core/services/maquinaria.service';
import { TipoMaquinariaService } from 'src/app/core/services/tipomaquinaria.service';
import { tipo } from 'src/app/core/models/tipo.model';
import { TipoMaterialService } from 'src/app/core/services/tipomaterial.service';



@Component({
    selector: 'app-material-list',
    templateUrl: './material-list.component.html',
    styleUrls: ['./material-list.component.scss']
})
/** material-list component*/
export class MaterialListComponent {

    public rows: tipo[];
    public temp: tipo[];
    public loading: boolean;

    constructor(public apiService: TipoMaterialService) { }


    @ViewChild(DatatableComponent) table: DatatableComponent;


    ngOnInit() {
        this.getAll();
    }

    getAll() {
        this.loading = true;

        this.apiService.getAll().subscribe(
            data => {
                this.rows = data;
                this.temp = [...data];
                this.loading = false;
            },
            () => this.loading = false);
    }



    delete(id: number) {
        if (confirm('Esta seguro de eliminar el registro?')) {
            this.apiService.delete(id).subscribe(() => {
                this.getAll();
            });
        }
    }
}

