
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { NovedadComponent } from './novedad.component';

let component: NovedadComponent;
let fixture: ComponentFixture<NovedadComponent>;

describe('novedad component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ NovedadComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(NovedadComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});
