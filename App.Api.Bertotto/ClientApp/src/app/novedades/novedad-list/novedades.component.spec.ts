
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { NovedadesComponent } from './novedades.component';

let component: NovedadesComponent;
let fixture: ComponentFixture<NovedadesComponent>;

describe('novedades component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ NovedadesComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(NovedadesComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});
