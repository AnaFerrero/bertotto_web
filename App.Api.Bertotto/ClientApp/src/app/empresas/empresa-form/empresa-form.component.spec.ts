﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { EmpresaFormComponent } from './empresa-form.component';

let component: EmpresaFormComponent;
let fixture: ComponentFixture<EmpresaFormComponent>;

describe('empresa-form component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EmpresaFormComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(EmpresaFormComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});