﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { EmpresaListComponent } from './empresa-list.component';

let component: EmpresaListComponent;
let fixture: ComponentFixture<EmpresaListComponent>;

describe('empresa-list component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EmpresaListComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(EmpresaListComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});