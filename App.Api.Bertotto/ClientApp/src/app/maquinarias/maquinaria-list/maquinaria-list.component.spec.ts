﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { MaquinariaListComponent } from './maquinaria-list.component';

let component: MaquinariaListComponent;
let fixture: ComponentFixture<MaquinariaListComponent>;

describe('maquinaria-list component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ MaquinariaListComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(MaquinariaListComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});