import { Component, ViewChild, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Maquinaria } from '../../core/models/maquinaria.model';
import { MaquinariaService } from '../../core/services/maquinaria.service';
import { TipoMaquinariaService } from 'src/app/core/services/tipomaquinaria.service';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
    selector: 'app-maquinaria-list',
    templateUrl: './maquinaria-list.component.html',
    styleUrls: ['./maquinaria-list.component.scss']
})
/** maquinaria-list component*/
export class MaquinariaListComponent {
    public rows: Maquinaria[];
    public temp: Maquinaria[];
    public loading: boolean;
    tipos: any[];
    formGroup: FormGroup;

    constructor(http: HttpClient, 
        private apiService: MaquinariaService,
        private fb: FormBuilder,
        private apiTipoService: TipoMaquinariaService) {
           
            this.formGroup = this.fb.group({
                'tipoId': [0]
            });
    
        }

    @ViewChild(DatatableComponent) table: DatatableComponent;



    ngOnInit() {

    document.body.style.backgroundColor = "#eee";
      

        this.apiTipoService.getAll().subscribe(
            data => {
                this.tipos = data;
            }
        )

        this.getAll();
    }
 get f() { return this.formGroup.controls; }


    getAll() {

        this.loading = true;

        this.apiService.getAll().subscribe(
            data => {
                this.rows = data;
                this.temp = [...data];
                this.loading = false;
            },
            error => this.loading = false);
  }

    getByTipo() {

        this.loading = true;

        let Id = this.formGroup.controls['tipoId'].value;

        this.apiService.GetActiveByTipo(Id).subscribe(
            data => {
                this.rows = data;
                this.temp = [...data];
                this.loading = false;
            },
            error => this.loading = false);
 }
 

    updateFilter(event) {

        const val = event.target.value.toLowerCase();

       
        const temp = this.temp.filter(function (d) {
            return d.nombre.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
        this.table.offset = 0;
    }


    delete(id: number) {
        if (confirm('Esta seguro de eliminar el registro?')) {
            this.apiService.delete(id).subscribe(res => {
                this.getAll();
            });
        }
    }
}

