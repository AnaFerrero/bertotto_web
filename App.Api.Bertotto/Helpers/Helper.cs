﻿using System;
using System.Drawing;
using System.IO;

namespace ClienteBB.Web.Helpers
{
    public static class Helper
    {
        public static string MakeRelative(string filePath, string referencePath)
        {
            var fileUri = new Uri(filePath);
            var referenceUri = new Uri(referencePath);
            return referenceUri.ToString();
            // return filePath.Replace("\",@'/')
            //return referenceUri.MakeRelativeUri(fileUri).ToString();
        }
        public static string GetBytes(string filePath)
        {
            try
            {
                using (Image image = Image.FromFile(filePath))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();

                        // Convert byte[] to Base64 String
                        string base64String = Convert.ToBase64String(imageBytes);
                        return "data:image/jpeg;base64," + base64String;
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public static string ConvertToImage(string image)
        {
            return "data:image/jpeg;base64," + image;

        }
        public static string GetNameUrl(string Nombre, int Id)
        {
            return Id.ToString() + "-" + Nombre.Replace(" ", "-").Replace(".", "").Replace(":", "");
        }

        public static Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }
    }
}
