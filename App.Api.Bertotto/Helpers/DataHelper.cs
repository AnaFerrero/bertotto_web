﻿using ClienteBB.Web.VModels;
using System.Collections.Generic;

namespace ClienteBB.Web.Helpers
{
    public static class DataHelper
    {

        public static List<ListVModel> GetTipoMaquinas()
        {
            List<ListVModel> tipo = new List<ListVModel>();

            tipo.Add(new ListVModel
            {
                Id = "Nueva",
                Nombre = "Nueva"
            });
            tipo.Add(new ListVModel
            {
                Id = "Usada",
                Nombre = "Usada"
            });
            return tipo;
        }
        public static List<ListVModel> GetProvincias()
        {
            List<ListVModel> provincias = new List<ListVModel>();

            provincias.Add(new ListVModel
            {
                Id = "Córdoba",
                Nombre = "Córdoba"
            });

            provincias.Add(new ListVModel
            {
                Id = "Buenos Aires",
                Nombre = "Buenos Aires"
            });
            provincias.Add(new ListVModel
            {
                Id = "Catamarca",
                Nombre = "Catamarca"
            });
            provincias.Add(new ListVModel
            {
                Id = "Chaco",
                Nombre = "Chaco"
            });
            provincias.Add(new ListVModel
            {
                Id = "Chubut",
                Nombre = "Chubut"
            });
            provincias.Add(new ListVModel
            {
                Id = "Corrientes",
                Nombre = "Corrientes"
            });
            provincias.Add(new ListVModel
            {
                Id = "Entre Ríos",
                Nombre = "Entre Ríos"
            });
            provincias.Add(new ListVModel
            {
                Id = "Formosa",
                Nombre = "Formosa"
            });
            provincias.Add(new ListVModel
            {
                Id = "Jujuy",
                Nombre = "Jujuy"
            });
            provincias.Add(new ListVModel
            {
                Id = "La Pampa",
                Nombre = "La Pampa"
            });
            provincias.Add(new ListVModel
            {
                Id = "La Rioja",
                Nombre = "La Rioja"
            });
            provincias.Add(new ListVModel
            {
                Id = "Mendoza",
                Nombre = "Mendoza"
            });
            provincias.Add(new ListVModel
            {
                Id = "Misiones",
                Nombre = "Misiones"
            });
            provincias.Add(new ListVModel
            {
                Id = "Neuquén",
                Nombre = "Neuquén"
            });
            provincias.Add(new ListVModel
            {
                Id = "Río Negro",
                Nombre = "Río Negro"
            });
            provincias.Add(new ListVModel
            {
                Id = "Río Negro",
                Nombre = "Río Negro"
            });
            provincias.Add(new ListVModel
            {
                Id = "Salta",
                Nombre = "Salta"
            });
            provincias.Add(new ListVModel
            {
                Id = "San Juan",
                Nombre = "San Juan"
            });
            provincias.Add(new ListVModel
            {
                Id = "Santa Cruz",
                Nombre = "Santa Cruz"
            });
            provincias.Add(new ListVModel
            {
                Id = "Santa Fe",
                Nombre = "Santa Fe"
            });
            provincias.Add(new ListVModel
            {
                Id = "Santiago del Estero",
                Nombre = "Santiago del Estero"
            });

            provincias.Add(new ListVModel
            {
                Id = "Tierra del Fuego",
                Nombre = "Tierra del Fuego"
            });
            provincias.Add(new ListVModel
            {
                Id = "Tucumán",
                Nombre = "Tucumán"
            });
            return provincias;
        }
    }
}
