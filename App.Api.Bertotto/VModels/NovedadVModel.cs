﻿using System;
using System.Collections.Generic;

namespace ClienteBB.Web.VModels
{
    public class NovedadVModel
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public string DescripcionCorta { get; set; }
        public string Descripcion { get; set; }

        public string Imagen { get; set; }
        public string MetaTitle { get; set; }

        public DateTime? Fecha_Alta { get; set; }

        public DateTime? Fecha_Desde_Notificacion { get; set; }

        public DateTime? Fecha_Hasta_Notificacion { get; set; }

        public Boolean Activo { get; set; }

        public int IdiomaId { get; set; }

        public List<GaleriaImagenVM> Imagenes { get; set; }
    }

    public class GaleriaImagenVM
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int NoticiaId { get; set; }

        public int MaquinariaId { get; set; }
        public int RepuestoId { get; set; }

        public bool Activa { get; set; }
        public bool Principal { get; set; }
        public string Imagen { get; set; }


    }
}
