﻿using System;
using System.Collections.Generic;

namespace ClienteBB.Web.VModels
{
    public class MaquinariaVModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string DescripcionCorta { get; set; }
        public string Descripcion { get; set; }
        public string UrlFolleto { get; set; }
        public string Modelo { get; set; }
        public string UrlManual { get; set; }
        public string MetaTitle { get; set; }
        public string TipoMaquina { get; set; }
        public string TipoMaterial { get; set; }
        public string IdiomaId { get; set; }
        public DateTime? Fecha { get; set; }
        public string Imagen { get; set; }
        public Boolean Activo { get; set; }
        public List<GaleriaImagenVM> Imagenes { get; set; }
    }
}
