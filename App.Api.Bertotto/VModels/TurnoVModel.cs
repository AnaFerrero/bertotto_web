﻿namespace ClienteBB.Web.VModels
{
    public class TurnoVModel
    {
        public int id { get; set; }
        public int usuarioId { get; set; }
        public string fecha { get; set; }
        public int hora { get; set; }
        public string imagen { get; set; }
        public int tipoMaquinaId { get; set; }
        public string descripcion { get; set; }
        public string nro_Chasis { get; set; }

    }
    public class FilterTurnoVModel
    {
        public string fecha_Desde { get; set; }
        public string fecha_Hasta { get; set; }
        public int estadoId { get; set; }
    }
}
