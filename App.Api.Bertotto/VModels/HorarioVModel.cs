﻿namespace ClienteBB.Web.VModels
{
    public class LoginVModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string cuit { get; set; }
        public string token { get; set; }
        public string rol { get; set; }
        public int totalNovedad { get; set; }

    }
}
