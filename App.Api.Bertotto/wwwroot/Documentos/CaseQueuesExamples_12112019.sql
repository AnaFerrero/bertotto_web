USE [EMR]
GO
SET IDENTITY_INSERT [dbo].[tSCB_caseQueues] ON 
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (60, 73931, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73931.pdf', NULL, N'
<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <font color="#0000ff">ANALIA FERRRERO</font></b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (61, 73930, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73930.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (62, 73935, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Approved', N'Pending', N'Case_73935.pdf', NULL, N'

<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (63, 73934, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73934.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (64, 73932, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73932.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (65, 73933, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73933.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (66, 73929, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73931.pdf', NULL, N'




<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class=WordSection1>

<p class=MsoTitle><span lang=X-NONE>CONFIDENTIAL</span></p>

<p class=MsoTitle><span lang=X-NONE>MEDICAL/PROGRESS REPORT</span></p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt''><b>PATIENT NAME:</b>                <b>            SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style=''margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto''><span
style=''font-weight:normal''>&nbsp;</span></h2>

<h2 style=''margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto''>DATE
OF BIRTH:                           April 25, 1930</h2>

<h2 style=''margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto''><span
style=''font-weight:normal''>&nbsp;</span></h2>

<h2 style=''margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto''>DATE
OF SERVICE:                                   November 18, 2019</h2>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''><b>HISTORY:</b>  This is a patient
who returns to the office today for followup regarding his right hip.  Overall
his hip is doing well.  He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.  He is at home and he has got no
problems, concerns or no other falls.</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b> 
Is otherwise reviewed.</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''><b>REVIEW OF SYSTEMS:</b>  As
stated above, otherwise noncontributory.</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''><b>PHYSICAL EXAMINATION</b>:  Examination
finds height is 5''8&quot;.  Weight is 180 lbs.  Respirations are 16.  He
ambulates with a walker.  His hips rotate well without discomfort.  His
incision is healing well.  He has good strength to resistance.  No calf pain. 
Foot is warm and well perfused.  Strength is intact.  He is neurovascularly
stable.</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''><b>RADIOGRAPHS:</b>  Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''><b>IMPRESSION:  </b>Right hip
fracture about a month out, overall doing well.</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''><b>PLAN:</b>  The patient is seen
today with his grandson.  He is doing great.  We are going to let him continue
to be weightbearing as tolerated.  We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''>&nbsp;</p>

<p class=MsoNormal style=''text-align:justify''>STEVEN A. ACKER, D.O.</p>

<p class=MsoNormal style=''text-align:justify''>BRITTANY MILLER, PA-C</p>

<p class=MsoNormal style=''text-align:justify''>SA:pwn:sib</p>

<p class=MsoNormal style=''text-align:justify''>DT:      11/20/2019</p>

<p class=MsoNormal style=''text-align:justify''>LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (67, 73928, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73928.pdf', NULL, N'

<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (68, 73927, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73927.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (69, 73926, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73926.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (70, 73925, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73925.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (71, 73924, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73924.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
INSERT [dbo].[tSCB_caseQueues] ([CaseQueueId], [caseId], [documentId], [userId], [timeStamp], [uploadedStamp], [uploaded], [StatusReview], [StatusSigning], [storage], [Description], [HTML]) VALUES (72, 73923, NULL, NULL, CAST(N'2019-12-10T17:07:00' AS SmallDateTime), CAST(N'2019-12-10T17:07:00' AS SmallDateTime), 1, N'Pending', N'Pending', N'Case_73923.pdf', NULL, N'


<title>CONFIDENTIAL</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h1
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:normal;}
h2
	{mso-style-link:"Título 2 Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
h3
	{margin:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-name:"Encabezado\, Char";
	mso-style-link:"Encabezado Car\, Char Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Pie de página Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Título Car";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Texto de globo Car";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma",sans-serif;}
p.1AutoList1, li.1AutoList1, div.1AutoList1
	{mso-style-name:1AutoList1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-36.0pt;
	text-autospace:none;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
span.CharCharChar
	{mso-style-name:"Char Char Char";}
span.hp
	{mso-style-name:hp;}
span.TtuloCar
	{mso-style-name:"Título Car";
	mso-style-link:Título;
	font-weight:bold;}
span.Ttulo2Car
	{mso-style-name:"Título 2 Car";
	mso-style-link:"Título 2";
	font-weight:bold;}
span.TextodegloboCar
	{mso-style-name:"Texto de globo Car";
	mso-style-link:"Texto de globo";
	font-family:"Tahoma",sans-serif;}
span.EncabezadoCar
	{mso-style-name:"Encabezado Car\, Char Car";
	mso-style-link:"Encabezado\, Char";}
span.PiedepginaCar
	{mso-style-name:"Pie de página Car";
	mso-style-link:"Pie de página";}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:612.0pt 792.0pt;
	margin:54.0pt 54.0pt 54.0pt 36.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>





<div class="WordSection1">

<p class="MsoTitle"><span lang="X-NONE">CONFIDENTIAL</span></p>

<p class="MsoTitle"><span lang="X-NONE">MEDICAL/PROGRESS REPORT</span></p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="margin-left:108.0pt;text-align:justify;text-indent:
-108.0pt"><b>PATIENT NAME:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHLOTZ, THOMAS (169784-LIVONIA)</b></p>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF BIRTH:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; April 25, 1930</h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto"><span style="font-weight:normal">&nbsp;</span></h2>

<h2 style="margin-left:108.0pt;text-indent:-108.0pt;page-break-after:auto">DATE
OF SERVICE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; November 18, 2019</h2>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>HISTORY:</b>&nbsp; This is a patient
who returns to the office today for followup regarding his right hip.&nbsp; Overall
his hip is doing well.&nbsp; He had a history of a right hip fracture that was fixed
just about a month ago and he is feeling good.&nbsp; He is at home and he has got no
problems, concerns or no other falls.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PAST MEDICAL HISTORY; PAST
SURGICAL HISTORY; MEDICATIONS; ALLERGIES; FAMILY HISTORY; AND SOCIAL HISTORY:</b>&nbsp;
Is otherwise reviewed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>REVIEW OF SYSTEMS:</b>&nbsp; As
stated above, otherwise noncontributory.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PHYSICAL EXAMINATION</b>:&nbsp; Examination
finds height is 5''8".&nbsp; Weight is 180 lbs.&nbsp; Respirations are 16.&nbsp; He
ambulates with a walker.&nbsp; His hips rotate well without discomfort.&nbsp; His
incision is healing well.&nbsp; He has good strength to resistance.&nbsp; No calf pain.&nbsp;
Foot is warm and well perfused.&nbsp; Strength is intact.&nbsp; He is neurovascularly
stable.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>RADIOGRAPHS:</b>&nbsp; Images are
obtained and reviewed of his hip showing right hip ORIF that is well fixed
without any backout or failure.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>IMPRESSION:&nbsp; </b>Right hip
fracture about a month out, overall doing well.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><b>PLAN:</b> &nbsp;The patient is seen
today with his grandson.&nbsp; He is doing great.&nbsp; We are going to let him continue
to be weightbearing as tolerated.&nbsp; We will plan to see him back in about two
months’ time, sooner if needed.</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">STEVEN A. ACKER, D.O.</p>

<p class="MsoNormal" style="text-align:justify">BRITTANY MILLER, PA-C</p>

<p class="MsoNormal" style="text-align:justify">SA:pwn:sib</p>

<p class="MsoNormal" style="text-align:justify">DT:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 11/20/2019</p>

<p class="MsoNormal" style="text-align:justify">LV034298</p>

</div>




')
GO
SET IDENTITY_INSERT [dbo].[tSCB_caseQueues] OFF
GO
